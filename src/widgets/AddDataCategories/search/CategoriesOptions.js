define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/query",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "./SearchComponent",
    "dojo/text!./templates/CategoriesOptions.html",
    "dojo/i18n!../nls/strings",
    "jimu/dijit/CheckBox",
    "dijit/TooltipDialog",
    "dijit/form/DropDownButton"
], function (
    declare,
    array,
    lang,
    on,
    query,
    domConstruct,
    domClass,
    domStyle,
    SearchComponent,
    template,
    i18n,
    CheckBox
) {
    return declare([SearchComponent], {
        i18n: i18n,
        templateString: template,
        optionsWidgets: [],

        postCreate: function () {
            this.inherited(arguments);
            // // this.own(on(this.categoriesDialog, "open", lang.hitch(this, async function () {
            // var cred = this.searchCategoriesPane.portal.credential;
            // var portal = cred.server
            // var token = cred.token
            // var cat = undefined
            // fetch(`${portal}/sharing/rest/portals/self/categorySchema?f=json&token=${token}`, {
            //     mode: 'cors'
            // })
            //     .then(response => {
            //         response.json()
            //     })
            //     .then(data => {
            //         cat = data.categorySchema[0].categories
            //         lang.hitch(this, this.initUI(cat));
            //     })
            //     .catch(err => {
            //         console.log('ERROR: ' + err);
            //     })

            // // if (this.searchCategoriesPane) {
            // //     var v = this.searchCategoriesPane.wabWidget.appConfig.theme.name;
            // // } else {
            // //     throw "No existe un search válido";
            // // }

            // // this.categoriesDialog.containerNode.innerHTML = "";

            // /*for (var [key, values] of Object.entries(this.searchCategoriesPane.resultsCategoriesPane.categoriesObj)) {
            //     if (key === "__nc") {
            //         var categoria = i18n.search.resultsCategoriesPane.noCat;
            //     } else {
            //         var categoria = key.replace("/Categories/", "");
            //     }

            //     var cb = new CheckBox({
            //         label: categoria,
            //         checked: true,
            //         data: key
            //     });

            //     this.optionsWidgets.push(cb);

            //     domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            //     cb.onChange = this.optionClicked;
            //     cb.placeAt(this.categoriesDialog);
            //     cb.startup();
            // }*/

            // // let cb_nc = new CheckBox({
            // //     label: i18n.search.resultsCategoriesPane.noCat,
            // //     checked: false,
            // //     data: "__nc"
            // // });
            // // this.optionsWidgets.push(cb_nc);

            // // domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            // // cb_nc.onChange = this.optionClicked;
            // // cb_nc.placeAt(this.categoriesDialog);
            // // cb_nc.startup();

            // // for (var i in cat) {
            // //     var cb = new CheckBox({
            // //         label: cat[i].title,
            // //         checked: false,
            // //         data: cat[i].title
            // //     });

            // //     this.optionsWidgets.push(cb);

            // //     domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            // //     cb.onChange = lang.hitch(this, this.optionClicked);
            // //     cb.placeAt(this.categoriesDialog);
            // //     cb.startup();
            // // }
            // // console.log(this.categoriesDialog._popupWrapper);
            // // console.log(this.categoriesDialog._popupWrapper.contentWidth);
            // // console.log(this.categoriesDialog._popupWrapper);
            // // })));
        },

        startup: function () {
            this.inherited(arguments);
            this.initOptions();
        },

        initOptions: function () {
            // this.own(on(this.categoriesDialog, "open", lang.hitch(this, async function () {
            var cred = this.searchCategoriesPane.portal.credential;
            var portal = cred.server
            var token = cred.token
            fetch(`${portal}/sharing/rest/portals/self/categorySchema?f=json&token=${token}`, {
                mode: 'cors'
            })
                .then(response => response.json())
                .then(data => {
                    let cat = data.categorySchema[0].categories
                    lang.hitch(this, this.initUI(cat));
                })
                .catch(err => {
                    console.log('ERROR: ' + err);
                })

            // console.log(cat);
            // if (this.searchCategoriesPane) {
            //     var v = this.searchCategoriesPane.wabWidget.appConfig.theme.name;
            // } else {
            //     throw "No existe un search válido";
            // }

            // this.categoriesDialog.containerNode.innerHTML = "";

            /*for (var [key, values] of Object.entries(this.searchCategoriesPane.resultsCategoriesPane.categoriesObj)) {
                if (key === "__nc") {
                    var categoria = i18n.search.resultsCategoriesPane.noCat;
                } else {
                    var categoria = key.replace("/Categories/", "");
                }
    
                var cb = new CheckBox({
                    label: categoria,
                    checked: true,
                    data: key
                });
    
                this.optionsWidgets.push(cb);
    
                domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");
    
                cb.onChange = this.optionClicked;
                cb.placeAt(this.categoriesDialog);
                cb.startup();
            }*/

            // let cb_nc = new CheckBox({
            //     label: i18n.search.resultsCategoriesPane.noCat,
            //     checked: false,
            //     data: "__nc"
            // });
            // this.optionsWidgets.push(cb_nc);

            // domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            // cb_nc.onChange = this.optionClicked;
            // cb_nc.placeAt(this.categoriesDialog);
            // cb_nc.startup();

            // for (var i in cat) {
            //     var cb = new CheckBox({
            //         label: cat[i].title,
            //         checked: false,
            //         data: cat[i].title
            //     });

            //     this.optionsWidgets.push(cb);

            //     domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            //     cb.onChange = lang.hitch(this, this.optionClicked);
            //     cb.placeAt(this.categoriesDialog);
            //     cb.startup();
            // }
            // console.log(this.categoriesDialog._popupWrapper);
            // console.log(this.categoriesDialog._popupWrapper.contentWidth);
            // console.log(this.categoriesDialog._popupWrapper);
            // })));
        },

        initUI: function (cat) {

            /*let cb_nc = new CheckBox({
                label: i18n.search.resultsCategoriesPane.noCat,
                checked: false,
                data: "_nc"
            });
            this.optionsWidgets.push(cb_nc);
            domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

            //cb.onChange = lang.hitch(this, this.optionClicked);
            cb_nc.onChange = this.optionClicked(this);
            cb_nc.placeAt(this.categoriesDialog);
            cb_nc.startup();*/

            for (var i in cat) {
                var cb = new CheckBox({
                    label: cat[i].title,
                    checked: false,
                    data: cat[i].title
                });

                this.optionsWidgets.push(cb);

                domStyle.set(this.categoriesDialog.containerNode, "display", "inline-grid");

                //cb.onChange = lang.hitch(this, this.optionClicked);
                cb.onChange = this.optionClicked(this);
                cb.placeAt(this.categoriesDialog);
                cb.startup();
            }
        },

        getOptionWidget: function () {
            return this.optionsWidgets;
        },

        optionClicked: function (ctx) {
            return function () {
                let arrCbChecked = query(this.domNode.parentNode).query('.jimu-checkbox').query('.jimu-icon-checked');
                if (arrCbChecked.length > 8) {
                    let e = query(this.domNode).query('.jimu-icon-checked');
                    e.addClass('jimu-icon-checkbox').removeClass('jimu-icon-checked');
                    this.checked = false;
                    console.log(i18n.search.categoriesOptions.maxLimit);
                    alert(i18n.search.categoriesOptions.maxLimit);
                }
                ctx.search();
            }
        },

        appendQueryParams: function (params) {

            function appendQ(q, qToAppend) {
                if (q.length > 0) {
                    q += ",";
                }
                return q + qToAppend;
            }

            var q = "",
                qAll = "",
                hasCheck = false;

            params.categories = null;
            array.forEach(this.getOptionWidget(), function (widget) {
                var dq = `${widget.data}`
                //qAll = appendQ(qAll, dq);
                qAll = '';
                if (widget.getValue()) {
                    if (dq != "_nc") {
                        q = appendQ(q, "/Categories/" + dq);
                        hasCheck = true;
                    } else {
                        q = "NOT /Categories AND " + q
                        hasCheck = true;
                    }
                }
            });
            if (!hasCheck) {
                q = qAll;
            }
            if (q !== null && q.length > 0) {
                // q = `(${q})`;
                q = `${q}`;
                if (params.categories !== null && params.categories.length > 0) {
                    // params.categories += `,${q}`;
                    params.categories.push(q);
                } else {
                    // params.categories = `categories:${q}`;
                    params.categories = [q];
                }
                // params.q += ` AND (${params.categories})`;
            }
        }
    });
});