define([
    'jimu/dijit/FeaturelayerChooserFromMap',
    'esri/geometry/Geometry',
    "esri/layers/CodedValueDomain",
    "esri/geometry/Point",
    'jimu/dijit/LoadingShelter',
    "dojo/dom-class",
    'dojo/on',
    "esri/map",
    'jimu/dijit/LayerChooserFromMapWithDropbox',
    'dojo/_base/declare',
    "esri/dijit/LayerList",
    "esri/arcgis/utils",
    'jimu/BaseWidget',
    'jimu/BaseWidgetSetting',
    'dojo/_base/lang',
    'jimu/LayerInfos/LayerInfos',
    'dojo/_base/array',
    "dojo/dom-attr",
    "esri/toolbars/draw",
    'esri/layers/GraphicsLayer',
    "esri/layers/FeatureLayer",
    "esri/tasks/QueryTask",
    "esri/tasks/query",
    "esri/graphic",
    "esri/graphicsUtils",
    'esri/geometry/Point',
    'esri/geometry/Polyline',
    'esri/SpatialReference',
    'esri/symbols/SimpleLineSymbol',
    'esri/symbols/SimpleFillSymbol',
    'esri/symbols/SimpleMarkerSymbol',
    'esri/Color',
    "esri/tasks/GeometryService",
    "esri/tasks/BufferParameters",
    "esri/tasks/ProjectParameters",
    "esri/tasks/DistanceParameters",
    'esri/dijit/editing/AttachmentEditor',
    "dojo/parser",
    "esri/geometry/Extent",
    'dojo/Deferred',
    'dojo/promise/all',
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-class",
    "dojo/domReady!"
],
    function (
        FeaturelayerChooserFromMap,
        Geometry,
        CodedValueDomain,
        Point,
        LoadingShelter,
        dojoClass,
        on,
        Map,
        LayerChooserFromMapWithDropbox,
        declare,
        LayerList,
        arcgisUtils,
        BaseWidget,
        BaseWidgetSetting,
        lang,
        LayerInfos,
        array,
        domAttr,
        Draw,
        GraphicsLayer,
        FeatureLayer,
        QueryTask,
        Query,
        Graphic,
        GraphicsUtils,
        Point,
        Polyline,
        SpatialReference,
        SimpleLineSymbol,
        SimpleFillSymbol,
        SimpleMarkerSymbol,
        Color,
        GeometryService,
        BufferParameters,
        ProjectParameters,
        DistanceParameters,
        AttachmentEditor,
        Parser,
        Extent,
        Deferred, 
        all,
        dom,
        DomConstruct,
        domStyle,
        domClass
    ) {
        return declare([BaseWidget], {
            baseClass: 'jimu-widget-creacion-incidencias-en-bloque',
            layerEntidades: [],
            layerIncidencias: [],
            tablaPos_base: [],
            tablaPos_ini: [],
            tablaPos_fin: [],
            draw: null,
            html_motivo: [],
            html_restriccion: [],
            url_incidencia: "",
            inc_grupo: "",
            inc_motivo: "",
            inc_prioridad: "",
            inc_descricion: "",
            inc_accion: "",
            inc_gestor: "",
            inc_notificacion: "",
            inc_d_territoriales: "",
            inc_d_cecopin: "",
            inc_restriccion: "",
            inc_res_finicio: "",
            inc_res_fin: "",
            inc_res_resolucion: "",
            inc_objectid: "",
            elementos_eq: [],
            selected_eq: [],
            selected_eq_f: [],
            centroideViario: [],
            Limitacion: [],
            LimitacionValue: [],
            Graphics_eq: null,
            glPuntoUTM: null,
            geometry_inc: "",
            graphics_globalid: [],
            Gestor: [],
            GestorValue: [],
            Territorio: [],
            TerritorioValue: [],
            Dotacion: [],
            DotacionValue: [],
            InsRec: [],
            InsRecValue: [],
            Arqueta: [],
            ArquetaValue: [],
            Deposito: [],
            DepositoValue: [],
            Barrera: [],
            BarreraValue: [],
            Via: [],
            ViaValue: [],
            ItinerarioAfectado: [],
            ItinerarioAfectadoValue: [],
            valores_seleccionados_Gestor: ")",
            valores_seleccionados_Territorio: ")",
            valores_seleccionados_Dotacion: ")",
            valores_seleccionados_InsRec: ")",
            valores_seleccionados_Arqueta: ")",
            valores_seleccionados_Deposito: ")",
            valores_seleccionados_Barrera: ")",
            valores_seleccionados_Via: ")",
            valores_seleccionados_ItinerarioAfectado: [],
            utm_x: 0,
            utm_y: 0,
            mode: '',
            resBuffer: null,
            loadingGif: null,

            onOpen: function () {

            },

            onClose: function () {
                this.draw.deactivate();
                if (document.getElementById('end_cib').style.display == 'block') {
                    document.getElementById('dialogo_exit_cib').style.display = 'none';
                    this.go_inicio();
                } else if (document.getElementById('inicio_cib').style.display == 'block') {
                    document.getElementById('dialogo_exit_cib').style.display = 'none';
                } else {
                    document.getElementById('dialogo_exit_cib').style.display = 'block';
                }
            },

            startup: function () {
                this.gsvc = new GeometryService(this.config.GeometryServer);
                this.Graphics_eq = new GraphicsLayer({ "id": 'glEquipamientos' });
                this.glPuntoUTM = new GraphicsLayer({ "id": "glPuntoUtm" });
                this.map.addLayer(this.Graphics_eq);
                this.map.addLayer(this.glPuntoUTM);
                this.inherited(arguments);
                var Grupo = [];
                var GrupoValue = [];
                var Motivo = [];
                var MotivoValue = [];
                var Prioridad = [];
                var PrioridadValue = [];
                var Accion = [];
                var AccionValue = [];
                var Booleano = [];
                var BooleanoValue = [];
                var BooleanoNotificacion = [];
                var BooleanoNotificacionValue = [];
                var Territoriales = [];
                var TerritorialesValue = [];
                var Transversales = [];
                var TransversalesValue = [];
                var Restriccion = [];
                var RestriccionValue = [];
                var NombreEntidades = [];
                var NombreEntidadesGrupo = [];

                let id = `${this.id}_panel`;
                let panelNode = dom.byId(id);
                let titleNode = panelNode.firstChild;
                let divLoading = DomConstruct.create('div');
                domClass.add(divLoading, this.baseClass);
                domClass.add(divLoading, "gifLoading");

                this.loadingGif = divLoading;

                DomConstruct.place(divLoading, titleNode, 'first');


                //Llamar al div del Grupo
                let divGrupo = document.getElementById("grupo_inc");
                let divGrupo_utm = document.getElementById("grupo_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Grupo)) {
                    Grupo.push(key);
                    GrupoValue.push(value);
                }
                //Rellenar el combo con los Grupos
                for (var i = 0; i < Grupo.length; i++) {
                    divGrupo.innerHTML += '<option value=' + GrupoValue[i].Valor + '>' + Grupo[i] + '</option>';
                    divGrupo_utm.innerHTML += '<option value=' + GrupoValue[i].Valor + '>' + Grupo[i] + '</option>';
                }

                //HTML Motivo
                //Rellenar los HTML de Motivo
                for (var i = 0; i < GrupoValue.length; i++) {
                    this.html_motivo[GrupoValue[i].Valor] = '';
                    for (var j = 0; j < GrupoValue[i].Motivo.length; j++) {
                        this.html_motivo[GrupoValue[i].Valor] += '<option value=' + GrupoValue[i].Motivo[j].Valor + '>' + GrupoValue[i].Motivo[j].Descripcion + '</option>';
                    }
                }

                //Llamar al div de Prioridad
                let divPrioridad = document.getElementById("prioridad_inc");
                let divPrioridad_utm = document.getElementById("prioridad_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Prioridad)) {
                    Prioridad.push(key);
                    PrioridadValue.push(value);
                }
                //Rellenar el combo con la Prioridad
                for (var i = 0; i < Prioridad.length; i++) {
                    divPrioridad.innerHTML += '<option value=' + PrioridadValue[i] + '>' + Prioridad[i] + '</option>';
                    divPrioridad_utm.innerHTML += '<option value=' + PrioridadValue[i] + '>' + Prioridad[i] + '</option>';
                }

                //Llamar al div de Accion
                let divAccion = document.getElementById("accion_inc");
                let divAccion_utm = document.getElementById("accion_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Accion)) {
                    Accion.push(key);
                    AccionValue.push(value);
                }
                //Rellenar el combo con la Accion
                for (var i = 0; i < Accion.length; i++) {
                    divAccion.innerHTML += '<option value=' + AccionValue[i] + '>' + Accion[i] + '</option>';
                    divAccion_utm.innerHTML += '<option value=' + AccionValue[i] + '>' + Accion[i] + '</option>';
                }

                //Llamar al div de Gestor
                let divGestor = document.getElementById("gestor_inc");
                let divGestor_utm = document.getElementById("gestor_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Gestor)) {
                    this.Gestor.push(key);
                    this.GestorValue.push(value);
                }
                //Rellenar el combo con el Gestor
                for (var i = 0; i < this.Gestor.length; i++) {
                    divGestor.innerHTML += '<option value=' + this.GestorValue[i] + '>' + this.Gestor[i] + '</option>';
                    divGestor_utm.innerHTML += '<option value=' + this.GestorValue[i] + '>' + this.Gestor[i] + '</option>';
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectGestor = document.getElementById("desplegableGestor")
                // var divMulstiselectGestor_utm = document.getElementById("desplegableGestor_utm")
                for (var i = 0; i < this.Gestor.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.GestorValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_ges" data-value="' + this.GestorValue[i] + '"class="checkInput checkbox Gestor" data="' + this.GestorValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Gestor[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableGestor', 'last');
                    // DomConstruct.place(div1, 'desplegableGestor_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Gestor MULTISELEC check
                var htmlCheck = document.getElementById("desplegableGestor")
                // var htmlCheck_utm = document.getElementById("desplegableGestor_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Territorio
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Territorio)) {
                    this.Territorio.push(key);
                    this.TerritorioValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectTerritorio = document.getElementById("desplegableTerritorio")
                // var divMulstiselectTerritorio_utm = document.getElementById("desplegableTerritorio_utm")
                for (var i = 0; i < this.Territorio.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.TerritorioValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_ter" data-value="' + this.TerritorioValue[i] + '"class="checkInput checkbox Territorio" data="' + this.TerritorioValue[i] + '">');
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Territorio[i] + '');
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableTerritorio', 'last');
                    // DomConstruct.place(div1, 'desplegableTerritorio_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Territorio MULTISELEC check
                var htmlCheck = document.getElementById("desplegableTerritorio")
                // var htmlCheck_utm = document.getElementById("desplegableTerritorio_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de dotacion
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Dotacion_tipo)) {
                    this.Dotacion.push(key);
                    this.DotacionValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectDotacion = document.getElementById("desplegableDotacion")
                // var divMulstiselectDotacion_utm = document.getElementById("desplegableDotacion_utm")
                for (var i = 0; i < this.Dotacion.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.DotacionValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_dot" data-value="' + this.DotacionValue[i] + '"class="checkInput checkbox Dotacion" data="' + this.DotacionValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Dotacion[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableDotacion', 'last');
                    // DomConstruct.place(div1, 'desplegableDotacion_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Dotacion MULTISELEC check
                var htmlCheck = document.getElementById("desplegableDotacion")
                // var htmlCheck_utm = document.getElementById("desplegableDotacion_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de instalacion recreativa
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Instalacion_tipo)) {
                    this.InsRec.push(key);
                    this.InsRecValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectInsRec = document.getElementById("desplegableInsRec")
                // var divMulstiselectInsRec_utm = document.getElementById("desplegableInsRec_utm")
                for (var i = 0; i < this.InsRec.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.InsRecValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_insrec" data-value="' + this.InsRecValue[i] + '"class="checkInput checkbox InsRec" data="' + this.InsRecValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.InsRec[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableInsRec', 'last');
                    // DomConstruct.place(div1, 'desplegableInsRec_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Instalacion recreativa MULTISELEC check
                var htmlCheck = document.getElementById("desplegableInsRec")
                // var htmlCheck_utm = document.getElementById("desplegableInsRec_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de arqueta
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Arqueta_tipo)) {
                    this.Arqueta.push(key);
                    this.ArquetaValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectInsRec = document.getElementById("desplegableArqueta")
                // var divMulstiselectInsRec_utm = document.getElementById("desplegableArqueta_utm")
                for (var i = 0; i < this.Arqueta.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.ArquetaValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_arq" data-value="' + this.ArquetaValue[i] + '"class="checkInput checkbox Arqueta" data="' + this.ArquetaValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Arqueta[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableArqueta', 'last');
                    // DomConstruct.place(div1, 'desplegableArqueta_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Arqueta MULTISELEC check
                var htmlCheck = document.getElementById("desplegableArqueta")
                // var htmlCheck_utm = document.getElementById("desplegableArqueta_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de Deposito
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Deposito_tipo)) {
                    this.Deposito.push(key);
                    this.DepositoValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectDeposito = document.getElementById("desplegableDeposito")
                // var divMulstiselectDeposito_utm = document.getElementById("desplegableDeposito_utm")
                for (var i = 0; i < this.Deposito.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.DepositoValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_dep" data-value="' + this.DepositoValue[i] + '"class="checkInput checkbox Deposito" data="' + this.DepositoValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Deposito[i] + '')
                    DomConstruct.place(div1, 'desplegableDeposito', 'last');
                    // DomConstruct.place(div1, 'desplegableDeposito_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Deposito MULTISELEC check
                var htmlCheck = document.getElementById("desplegableDeposito")
                // var htmlCheck_utm = document.getElementById("desplegableDeposito_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de Barrera
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Barrera_tipo)) {
                    this.Barrera.push(key);
                    this.BarreraValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectBarrera = document.getElementById("desplegableBarrera")
                // var divMulstiselectBarrera_utm = document.getElementById("desplegableBarrera_utm")
                for (var i = 0; i < this.Barrera.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.BarreraValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_bar" data-value="' + this.BarreraValue[i] + '"class="checkInput checkbox Barrera" data="' + this.BarreraValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Barrera[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableBarrera', 'last');
                    // DomConstruct.place(div1, 'desplegableBarrera_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Barrera MULTISELEC check
                var htmlCheck = document.getElementById("desplegableBarrera")
                // var htmlCheck_utm = document.getElementById("desplegableBarrera_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div de Tipo de Via
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Via_tipo)) {
                    this.Via.push(key);
                    this.ViaValue.push(value);
                }
                //Crear div MULTISELECT dropdown
                var divMulstiselectVia = document.getElementById("desplegableVia")
                // var divMulstiselectVia_utm = document.getElementById("desplegableVia_utm")
                for (var i = 0; i < this.Via.length; i++) {
                    var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.ViaValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                    var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_via" data-value="' + this.ViaValue[i] + '"class="checkInput checkbox Via" data="' + this.ViaValue[i] + '">')
                    var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.Via[i] + '')
                    // on(div2, 'click', lang.hitch(this, this._checking(this)));
                    DomConstruct.place(div1, 'desplegableVia', 'last');
                    // DomConstruct.place(div1, 'desplegableVia_utm', 'last');
                    DomConstruct.place(div2, div1, 'last');
                    DomConstruct.place(div3, div1, 'last');
                }
                //Addeventlister Tipo Via MULTISELEC check
                var htmlCheck = document.getElementById("desplegableVia")
                // var htmlCheck_utm = document.getElementById("desplegableVia_utm")
                htmlCheck.addEventListener('click', (event) => {
                    this._checking(event)
                });

                //Llamar al div del Notificacion
                let divNotificacion = document.getElementById("notificacion_inc");
                let divNotificacion_utm = document.getElementById("notificacion_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.BooleanoNotificacion)) {
                    BooleanoNotificacion.push(key);
                    BooleanoNotificacionValue.push(value);
                }
                //Rellenar el combo con la Notificacion 
                for (var i = 0; i < BooleanoNotificacion.length; i++) {
                    divNotificacion.innerHTML += '<option value=' + BooleanoNotificacionValue[i] + '>' + BooleanoNotificacion[i] + '</option>';
                    divNotificacion_utm.innerHTML += '<option value=' + BooleanoNotificacionValue[i] + '>' + BooleanoNotificacion[i] + '</option>';
                }

                //Llamar al div del Cecopin
                let divCecopin = document.getElementById("cecopin_inc");
                let divCecopin_utm = document.getElementById("cecopin_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Booleano)) {
                    Booleano.push(key);
                    BooleanoValue.push(value);
                }
                //Rellenar el combo con la Cecopin
                for (var i = 0; i < Booleano.length; i++) {
                    divCecopin.innerHTML += '<option value=' + BooleanoValue[i] + '>' + Booleano[i] + '</option>';
                    divCecopin_utm.innerHTML += '<option value=' + BooleanoValue[i] + '>' + Booleano[i] + '</option>';
                }

                //Llamar al div de D_Territoriales
                let divTerritoriales = document.getElementById("territoriales_inc");
                let divTerritoriales_utm = document.getElementById("territoriales_inc_utm");
                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.U_Territoriales)) {
                    Territoriales.push(key);
                    TerritorialesValue.push(value);
                }
                //Rellenar el combo con los D_Territoriales
                for (var i = 0; i < Territoriales.length; i++) {
                    divTerritoriales.innerHTML += '<option value=' + TerritorialesValue[i] + '>' + Territoriales[i] + '</option>';
                    divTerritoriales_utm.innerHTML += '<option value=' + TerritorialesValue[i] + '>' + Territoriales[i] + '</option>';
                }

                //Obtener los keys de las entidades del Config				
                for (const [key, value] of Object.entries(this.config.Restriccion)) {
                    Restriccion.push(key);
                    RestriccionValue.push(value);
                }
                //HTML Restriccion
                //Rellenar los HTML de Restriccion
                for (var i = 0; i < RestriccionValue.length; i++) {
                    for (var j = 0; j < RestriccionValue[i].Grupo.length; j++) {
                        if (this.html_restriccion[RestriccionValue[i].Grupo[j]] == undefined) {
                            this.html_restriccion[RestriccionValue[i].Grupo[j]] = '';
                        }
                        this.html_restriccion[RestriccionValue[i].Grupo[j]] += '<option value=' + RestriccionValue[i].Valor + '>' + Restriccion[i] + '</option>';
                    }
                }

                //Llamar al div de Tipo de Itinerario afectado (Restriccion)
                //Obtener los keys de las entidades del Config
                
                //URL de Incidencia
                for (var j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
                    if (this.map.itemInfo.itemData.operationalLayers[j].title == this.config.Incidencia) {
                        this.url_incidencia = this.map.itemInfo.itemData.operationalLayers[j].url;
                        break;
                    }
                }

                var deferred = new Deferred();
;                
                var capaIncidencias = new FeatureLayer(this.url_incidencia);

                capaIncidencias.on("load", function(){
                    deferred.resolve(capaIncidencias)
                    var DomainItinerarios = capaIncidencias.getDomain("restriccion_itinerarios")
                    console.log(DomainItinerarios)
                    this.ItinerarioAfectado = [];
                    this.ItinerarioAfectadoValue = [];
                    for(var x=0; x < DomainItinerarios.codedValues.length; x++){
                        this.ItinerarioAfectado.push(DomainItinerarios.codedValues[x].name);
                        this.ItinerarioAfectadoValue.push(DomainItinerarios.codedValues[x].code);
                    }

                    //Crear div MULTISELECT dropdown
                    var divMulstiselectItiAfec = document.getElementById("desplegableItinerarioAfectado")
                    var divMulstiselectItiAfec_utm = document.getElementById("desplegableItinerarioAfectado_utm")
                    for (var i = 0; i < this.ItinerarioAfectado.length; i++) {
                        var div1 = DomConstruct.toDom('<div class="item" data-value="' + this.ItinerarioAfectadoValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                        var div2 = DomConstruct.toDom('<div id="seleccionable_' + i + '_iteafec" data-value="' + this.ItinerarioAfectadoValue[i] + '"class="checkInput checkbox ItinerarioAfectado" data="' + this.ItinerarioAfectadoValue[i] + '">')
                        var div3 = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.ItinerarioAfectado[i] + '')

                        var div1_utm = DomConstruct.toDom('<div class="item" data-value="' + this.ItinerarioAfectadoValue[i] + '" role="option" tabindex="-1" aria-selected="true" aria-checked="false">');
                        var div2_utm = DomConstruct.toDom('<div id="seleccionable_' + i + '_iteafec_utm" data-value="' + this.ItinerarioAfectadoValue[i] + '"class="checkInput checkbox ItinerarioAfectado" data="' + this.ItinerarioAfectadoValue[i] + '">')
                        var div3_utm = DomConstruct.toDom('<div class="label jimu-ellipsis-Blanks labelRuntime" style="max-width: 284.8px;">' + this.ItinerarioAfectado[i] + '')

                        DomConstruct.place(div1, 'desplegableItinerarioAfectado', 'last');
                        DomConstruct.place(div2, div1, 'last');
                        DomConstruct.place(div3, div1, 'last');

                        DomConstruct.place(div1_utm, 'desplegableItinerarioAfectado_utm', 'last');
                        DomConstruct.place(div2_utm, div1_utm, 'last');
                        DomConstruct.place(div3_utm, div1_utm, 'last');
                    }
                    });
                    //Addeventlister Tipo Itinerario afectado MULTISELEC check
                    var htmlCheck = document.getElementById("desplegableItinerarioAfectado")
                    var htmlCheck_utm = document.getElementById("desplegableItinerarioAfectado_utm")
                    htmlCheck.addEventListener('click', (event) => {
                        this._checking(event)
                    });
                    htmlCheck_utm.addEventListener('click', (event) => {
                        this._checking(event)
                    });

                capaIncidencias.on('error', function(error) {
                    deferred.reject(error);
                  });
                

                //Entidades relacionadas con incidencias
                for (const [key, value] of Object.entries(this.config.CapasIncidenciasGrupo)) {
                    NombreEntidades.push(key);
                    NombreEntidadesGrupo.push(value);
                }
                this.cont = 0;
                for (var i = 0; i < NombreEntidades.length; i++) {
                    for (var j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
                        if (this.map.itemInfo.itemData.operationalLayers[j].title == NombreEntidades[i]) {
                            this.layerEntidades[this.cont] = new this.Relations(this.map.itemInfo.itemData.operationalLayers[j].url, this.map.itemInfo.itemData.operationalLayers[j].id, this.map.itemInfo.itemData.operationalLayers[j].title, NombreEntidadesGrupo[i]);
                            this.cont++;
                        }
                    }
                }

                //URL de Incidencia
                for (var j = 0; j < this.map.itemInfo.itemData.operationalLayers.length; j++) {
                    if (this.map.itemInfo.itemData.operationalLayers[j].title == this.config.Incidencia) {
                        this.url_incidencia = this.map.itemInfo.itemData.operationalLayers[j].url;
                        break;
                    }
                }

                //Boton Comenzar
                var button_fc = document.getElementById('comenzar_cib');
                button_fc.addEventListener('click', () => { 
                    this.mode = 'cib';
                    this.load_page(1);
                });
                var button_fc_utm = document.getElementById('comenzar_utm');
                button_fc_utm.addEventListener('click', () => {
                    this.mode = 'utm';
                    this.load_page(6);
                });

                //Button Combo Grupo	
                var grupobutton = document.getElementById("grupo_inc");
                grupobutton.addEventListener("change", () => {
                    var GrupoSeleccionado = parseInt(document.getElementById("grupo_inc").value);
                    document.getElementById("motivo_inc").innerHTML = '';
                    document.getElementById("motivo_inc").innerHTML = this.html_motivo[GrupoSeleccionado];
                    document.getElementById("restricciones_inc").innerHTML = '';
                    document.getElementById("restricciones_inc").innerHTML = this.html_restriccion[GrupoSeleccionado];
                    document.getElementById('entidad_list').innerHTML = '';
                    document.getElementById("titulo_finicio").style.display = "none";
                    document.getElementById("res_finicio").style.display = "none";
                    document.getElementById("titulo_ffin").style.display = "none";
                    document.getElementById("res_ffin").style.display = "none";
                    document.getElementById("enlaceResParrafo").style.display = "none";
                    document.getElementById("res_enlace").style.display = "none";
                    document.getElementById("MultiselecAfectaItine").style.display = "none";
                    this.elementos_eq = [];
                    this.Graphics_eq.clear();
                    if (this.config.GestorVisible.includes(GrupoSeleccionado) == true) {
                        document.getElementById('linea_separadora').style.display = 'block';
                        document.getElementById('titulo_gestor').style.display = 'block';
                        document.getElementById('campo_gestor').style.display = 'block';
                        document.getElementById('gestor_inc').style.display = 'block';
                    } else {
                        document.getElementById('linea_separadora').style.display = 'none';
                        document.getElementById('titulo_gestor').style.display = 'none';
                        document.getElementById('campo_gestor').style.display = 'none';
                        document.getElementById('gestor_inc').style.display = 'none';
                    }
                });

                var grupobutton_utm = document.getElementById("grupo_inc_utm");
                grupobutton_utm.addEventListener("change", () => {
                    var GrupoSeleccionado = parseInt(document.getElementById("grupo_inc_utm").value);
                    document.getElementById("motivo_inc_utm").innerHTML = '';
                    document.getElementById("motivo_inc_utm").innerHTML = this.html_motivo[GrupoSeleccionado];
                    document.getElementById("restricciones_inc_utm").innerHTML = '';
                    document.getElementById("restricciones_inc_utm").innerHTML = this.html_restriccion[GrupoSeleccionado];
                    document.getElementById('entidad_list_utm').innerHTML = '';
                    document.getElementById("titulo_finicio_utm").style.display = "none";
                    document.getElementById("res_finicio_utm").style.display = "none";
                    document.getElementById("titulo_ffin_utm").style.display = "none";
                    document.getElementById("res_ffin_utm").style.display = "none";
                    document.getElementById("enlaceResParrafo_utm").style.display = "none";
                    document.getElementById("res_enlace_utm").style.display = "none";
                    document.getElementById("MultiselecAfectaItine_utm").style.display = "none";
                    this.elementos_eq = [];
                    this.Graphics_eq.clear();
                    if (this.config.GestorVisible.includes(GrupoSeleccionado) == true) {
                        document.getElementById('linea_separadora').style.display = 'block';
                        document.getElementById('titulo_gestor_utm').style.display = 'block';
                        document.getElementById('campo_gestor_utm').style.display = 'block';
                        document.getElementById('gestor_inc_utm').style.display = 'block';
                    } else {
                        document.getElementById('linea_separadora').style.display = 'none';
                        document.getElementById('titulo_gestor_utm').style.display = 'none';
                        document.getElementById('campo_gestor_utm').style.display = 'none';
                        document.getElementById('gestor_inc_utm').style.display = 'none';
                    }
                });

                //Desplegar los divs de Generacion de notificaciones
                var notificacionbutton = document.getElementById("notificacion_inc")
                notificacionbutton.addEventListener("change", () => {
                    if (document.getElementById("notificacion_inc").value == "1") {
                        document.getElementById("utParrafo").style.display = "block"
                        document.getElementById("territoriales_inc").style.display = "block"
                        document.getElementById("cecopinParrafo").style.display = "block"
                        document.getElementById("cecopin_inc").style.display = "block"
                    } else if (document.getElementById("notificacion_inc").value == "0") {
                        document.getElementById("utParrafo").style.display = "none"
                        document.getElementById("territoriales_inc").style.display = "none"
                        document.getElementById("cecopinParrafo").style.display = "none"
                        document.getElementById("cecopin_inc").style.display = "none"
                    }
                });

                //Desplegar los divs de Restricciones
                var restriccionbutton = document.getElementById("restricciones_inc")
                restriccionbutton.addEventListener("change", () => {
                    if (document.getElementById("restricciones_inc").value == "1" || document.getElementById("restricciones_inc").value == "2") {
                        document.getElementById("titulo_finicio").style.display = "block"
                        document.getElementById("res_finicio").style.display = "block"
                        document.getElementById("titulo_ffin").style.display = "block"
                        document.getElementById("res_ffin").style.display = "block"
                        document.getElementById("enlaceResParrafo").style.display = "block"
                        document.getElementById("res_enlace").style.display = "block"
                        document.getElementById("MultiselecAfectaItine").style.display = "block"
                    } else if (document.getElementById("restricciones_inc").value == "0") {
                        document.getElementById("titulo_finicio").style.display = "none"
                        document.getElementById("res_finicio").style.display = "none"
                        document.getElementById("titulo_ffin").style.display = "none"
                        document.getElementById("res_ffin").style.display = "none"
                        document.getElementById("enlaceResParrafo").style.display = "none"
                        document.getElementById("res_enlace").style.display = "none"
                        document.getElementById("MultiselecAfectaItine").style.display = "none"
                    } else if (document.getElementById("restricciones_inc").value == "3") {
                        document.getElementById("titulo_finicio").style.display = "block"
                        document.getElementById("res_finicio").style.display = "block"
                        document.getElementById("titulo_ffin").style.display = "none"
                        document.getElementById("res_ffin").style.display = "none"
                        document.getElementById("enlaceResParrafo").style.display = "block"
                        document.getElementById("res_enlace").style.display = "block"
                        document.getElementById("MultiselecAfectaItine").style.display = "block"
                    }
                });

                var restriccionbutton = document.getElementById("restricciones_inc_utm")
                restriccionbutton.addEventListener("change", () => {
                    if (document.getElementById("restricciones_inc_utm").value == "1" || document.getElementById("restricciones_inc_utm").value == "2") {
                        document.getElementById("titulo_finicio_utm").style.display = "block"
                        document.getElementById("res_finicio_utm").style.display = "block"
                        document.getElementById("titulo_ffin_utm").style.display = "block"
                        document.getElementById("res_ffin_utm").style.display = "block"
                        document.getElementById("enlaceResParrafo_utm").style.display = "block"
                        document.getElementById("res_enlace_utm").style.display = "block"
                        document.getElementById("MultiselecAfectaItine_utm").style.display = "block"
                    } else if (document.getElementById("restricciones_inc_utm").value == "0") {
                        document.getElementById("titulo_finicio_utm").style.display = "none"
                        document.getElementById("res_finicio_utm").style.display = "none"
                        document.getElementById("titulo_ffin_utm").style.display = "none"
                        document.getElementById("res_ffin_utm").style.display = "none"
                        document.getElementById("enlaceResParrafo_utm").style.display = "none"
                        document.getElementById("res_enlace_utm").style.display = "none"
                        document.getElementById("MultiselecAfectaItine_utm").style.display = "none"
                    } else if (document.getElementById("restricciones_inc").value == "3") {
                        document.getElementById("titulo_finicio").style.display = "block"
                        document.getElementById("res_finicio").style.display = "block"
                        document.getElementById("titulo_ffin").style.display = "none"
                        document.getElementById("res_ffin").style.display = "none"
                        document.getElementById("enlaceResParrafo").style.display = "block"
                        document.getElementById("res_enlace").style.display = "block"
                        document.getElementById("MultiselecAfectaItine").style.display = "block"
                    }
                });

                //Boton Siguiente Formulario_1
                var button_f1 = document.getElementById('end_form_1_cib');
                button_f1.addEventListener('click', () => {
                    var aux = false;
                    this.inc_grupo = document.getElementById('grupo_inc').value;
                    this.inc_motivo = document.getElementById('motivo_inc').value;
                    this.inc_prioridad = document.getElementById('prioridad_inc').value;
                    this.inc_descricion = document.getElementById('description_inc').value;
                    this.inc_accion = document.getElementById('accion_inc').value;
                    this.inc_gestor = document.getElementById('gestor_inc').value;
                    this.inc_notificacion = document.getElementById('notificacion_inc').value;
                    this.inc_d_territoriales = document.getElementById('territoriales_inc').value;
                    this.inc_d_cecopin = document.getElementById('cecopin_inc').value;
                    this.inc_restriccion = document.getElementById('restricciones_inc').value;
                    this.inc_res_finicio = document.getElementById('res_finicio').value;
                    this.inc_res_fin = document.getElementById('res_ffin').value;
                    this.inc_res_resolucion = document.getElementById('res_enlace').value;
                    document.getElementById('grupo_oblig').style.display = 'none';
                    document.getElementById('descripcion_oblig').style.display = 'none';
                    document.getElementById('dropdownDotacion').style.display = 'none';
                    document.getElementById('dropdownInsRec').style.display = 'none';
                    document.getElementById('dropdownArqueta').style.display = 'none';
                    document.getElementById('dropdownDeposito').style.display = 'none';
                    document.getElementById('dropdownBarrera').style.display = 'none';
                    document.getElementById('dropdownVia').style.display = 'none';
                    if (this.inc_grupo == '') {
                        document.getElementById('grupo_oblig').style.display = 'block';
                        aux = true;
                    }
                    if (this.inc_descricion == '') {
                        document.getElementById('descripcion_oblig').style.display = 'block';
                        aux = true;
                    }
                    if (aux == false) {
                        document.getElementById('grupo_oblig').style.display = 'none';
                        document.getElementById('descripcion_oblig').style.display = 'none';
                        document.getElementById('geometry_oblig').style.display = 'none';
                        document.getElementById('glayer_novisible').style.display = 'none';
                        document.getElementById('select_region').style.display = 'none';
                        document.getElementById('least_one_list').style.display = 'none';
                        this.load_page(2);
                    }
                    // MULTISELECT en función del grupo de dotacion elegido
                    if (this.inc_grupo == '31') {
                        document.getElementById('dropdownDotacion').style.display = 'block';
                    }
                    // MULTISELECT en función del grupo de instalacion recreativa elegido
                    if (this.inc_grupo == '3') {
                        document.getElementById('dropdownInsRec').style.display = 'block';
                    }
                    // MULTISELECT en función del grupo de arqueta elegido
                    if (this.inc_grupo == '7') {
                        document.getElementById('dropdownArqueta').style.display = 'block';
                    }
                    // MULTISELECT en función del grupo de deposito elegido
                    if (this.inc_grupo == '8') {
                        document.getElementById('dropdownDeposito').style.display = 'block';
                    }
                    // MULTISELECT en función del grupo de barrera elegido
                    if (this.inc_grupo == '9') {
                        document.getElementById('dropdownBarrera').style.display = 'block';
                    }
                    // MULTISELECT en función del grupo de viario elegido
                    if (this.inc_grupo == '5') {
                        document.getElementById('dropdownVia').style.display = 'block';
                    }
                });

                //Boton Siguiente Formulario_1_UTM
                var button_f1_utm = document.getElementById('end_form_1_utm');
                button_f1_utm.addEventListener('click', () => {
                    var aux = false;
                    this.utm_x = document.getElementById('input_utm_x').value;;
                    this.utm_y = document.getElementById('input_utm_y').value;;
                    this.inc_grupo = document.getElementById('grupo_inc_utm').value;
                    this.inc_motivo = document.getElementById('motivo_inc_utm').value;
                    this.inc_prioridad = document.getElementById('prioridad_inc_utm').value;
                    this.inc_descricion = document.getElementById('description_inc_utm').value;
                    this.inc_accion = document.getElementById('accion_inc_utm').value;
                    this.inc_gestor = document.getElementById('gestor_inc_utm').value;
                    this.inc_notificacion = document.getElementById('notificacion_inc_utm').value;
                    this.inc_d_territoriales = document.getElementById('territoriales_inc_utm').value;
                    this.inc_d_cecopin = document.getElementById('cecopin_inc_utm').value;
                    this.inc_restriccion = document.getElementById('restricciones_inc_utm').value;
                    this.inc_res_finicio = document.getElementById('res_finicio_utm').value;
                    this.inc_res_fin = document.getElementById('res_ffin_utm').value;
                    this.inc_res_resolucion = document.getElementById('res_enlace_utm').value;
                    document.getElementById('grupo_oblig_utm').style.display = 'none';
                    document.getElementById('descripcion_oblig_utm').style.display = 'none';
                    // document.getElementById('dropdownDotacion_utm').style.display = 'none';
                    // document.getElementById('dropdownInsRec_utm').style.display = 'none';
                    // document.getElementById('dropdownArqueta_utm').style.display = 'none';
                    // document.getElementById('dropdownDeposito_utm').style.display = 'none';
                    // document.getElementById('dropdownBarrera_utm').style.display = 'none';
                    // document.getElementById('dropdownVia_utm').style.display = 'none';
                    if (this.inc_grupo == '') {
                        document.getElementById('grupo_oblig_utm').style.display = 'block';
                        aux = true;
                    }
                    if (this.inc_descricion == '') {
                        document.getElementById('descripcion_oblig_utm').style.display = 'block';
                        aux = true;
                    }
                    if (aux == false) {
                        document.getElementById('grupo_oblig_utm').style.display = 'none';
                        document.getElementById('descripcion_oblig_utm').style.display = 'none';
                        document.getElementById('geometry_oblig_utm').style.display = 'none';
                        document.getElementById('glayer_novisible_utm').style.display = 'none';
                        document.getElementById('select_region_utm').style.display = 'none';
                        document.getElementById('least_one_list_utm').style.display = 'none';
                        //this.load_page(6);
                    }
                    

                    let sr_utm = new SpatialReference(this.config.utm_search.sr_wkid);
                    let geometria = new Point(this.utm_x, this.utm_y, sr_utm);
                    let params = new ProjectParameters();
                    params.geometries = [geometria];
                    params.outSR = this.map.spatialReference;
                    if (this.config.utm_search.transformation_wkid !== null) {
                        params.transformation.wkid = this.config.utm_search.transformation_wkid;
                    }
                    this.loading();
                    this.gsvc.project(params, (geometries) => {
                        if (geometries.length > 0) {
                            let geometria_pjt = geometries[0]
                            this.geoIndicencia_utm(geometria_pjt);
                        } else {
                            this.finLoading();
                            alert('La proyección no ha dado ningún resultado');
                            this.load_page(6);
                        }
                    }, (err) => {
                        console.log(err);
                        this.finLoading();
                        alert('Error al proyectar la geometría');
                        this.load_page(6);
                    });
                });

                //Boton Draw
                var draw_btn = document.getElementById('select_polygon');
                draw_btn.onclick = lang.hitch(this, this.botonDraw);

                this.draw = new Draw(this.map);
                this.draw.on('draw-complete', lang.hitch(this, this.geoIndicencia));

                //Boton Siguiente Formulario_2
                var button_f2 = document.getElementById('end_form_23_cib');
                button_f2.addEventListener('click', () => {
                    this.selected_eq = [];
                    for (var i = 0; i < this.elementos_eq.length; i++) {
                        var n = i.toString();
                        var id = 'check_btn_' + n;
                        if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
                            this.selected_eq.push(this.elementos_eq[i]);
                        }
                    }
                    if (this.selected_eq.length == 0) {
                        document.getElementById('least_one_list').style.display = 'block';
                    } else {
                        document.getElementById('least_one_list').style.display = 'none';
                        document.getElementById('least_one_list_select').style.display = 'none';
                        this.load_page(3);
                        this.Graphics_eq.clear();
                        for (var i = 0; i < this.layerIncidencias.length; i++) {
                            var globalid_selected = "globalid IN (";
                            for (var j = 0; j < this.selected_eq.length; j++) {
                                if (i == this.selected_eq[j].index) {
                                    globalid_selected += "'" + this.selected_eq[j].features.attributes.globalid + "',";
                                }
                            }
                            globalid_selected += "'')";
                            var id_eq;
                            for (var k = 0; k < this.layerEntidades.length; k++) {
                                if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                    id_eq = this.layerEntidades[k].related_layers_id;
                                }
                            }
                            var query = new Query();
                            query.where = globalid_selected;
                            query.outFields = ["*"];
                            query.returnGeometry = true;
                            this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                                for (var i = 0; i < result_select_graphics.length; i++) {
                                    if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                        var line = new SimpleLineSymbol();
                                        line.setWidth(5);
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        result_select_graphics[i].symbol = line;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                        if (i == result_select_graphics.length - 1) {
                                            this.Graphics_eq.clear();
                                            for (var i = 0; i < result_select_graphics.length; i++) {
                                                this.Graphics_eq.add(result_select_graphics[i]);
                                            }
                                        }
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        var marker = new SimpleMarkerSymbol();
                                        marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                        marker.setColor(new Color([0, 197, 255, 0.26]));
                                        marker.setAngle(0);
                                        marker.setOutline(line);
                                        result_select_graphics[i].symbol = marker;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                }
                                this.Graphics_eq.redraw();
                            }, (rollback_select_graphics) => {
                                this.Graphics_eq.clear();
                            });
                        }
                        this.load_table_selected();
                    }
                });

                //Boton Siguiente Formulario_2_UTM
                var button_f2_utm = document.getElementById('end_form_23_utm');
                button_f2_utm.addEventListener('click', () => {
                    this.selected_eq = [];
                    for (var i = 0; i < this.elementos_eq.length; i++) {
                        var n = i.toString();
                        var id = 'check_btn_' + n;
                        if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
                            this.selected_eq.push(this.elementos_eq[i]);
                        }
                    }
                    if (this.selected_eq.length == 0) {
                        document.getElementById('least_one_list_utm').style.display = 'block';
                    } else {
                        document.getElementById('least_one_list_utm').style.display = 'none';
                        document.getElementById('least_one_list_select_utm').style.display = 'none';
                        this.load_page(8);
                        this.Graphics_eq.clear();
                        for (var i = 0; i < this.layerIncidencias.length; i++) {
                            var globalid_selected = "globalid IN (";
                            for (var j = 0; j < this.selected_eq.length; j++) {
                                if (i == this.selected_eq[j].index) {
                                    globalid_selected += "'" + this.selected_eq[j].features.attributes.globalid + "',";
                                }
                            }
                            globalid_selected += "'')";
                            var id_eq;
                            for (var k = 0; k < this.layerEntidades.length; k++) {
                                if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                    id_eq = this.layerEntidades[k].related_layers_id;
                                }
                            }
                            var query = new Query();
                            query.where = globalid_selected;
                            query.outFields = ["*"];
                            query.returnGeometry = true;
                            this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                                for (var i = 0; i < result_select_graphics.length; i++) {
                                    if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                        var line = new SimpleLineSymbol();
                                        line.setWidth(5);
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        result_select_graphics[i].symbol = line;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                        if (i == result_select_graphics.length - 1) {
                                            this.Graphics_eq.clear();
                                            for (var i = 0; i < result_select_graphics.length; i++) {
                                                this.Graphics_eq.add(result_select_graphics[i]);
                                            }
                                        }
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        var marker = new SimpleMarkerSymbol();
                                        marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                        marker.setColor(new Color([0, 197, 255, 0.26]));
                                        marker.setAngle(0);
                                        marker.setOutline(line);
                                        result_select_graphics[i].symbol = marker;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                }
                                this.Graphics_eq.redraw();
                            }, (rollback_select_graphics) => {
                                this.Graphics_eq.clear();
                            });
                        }
                        this.load_table_selected_utm();
                    }
                });

                //Boton Atras Formulario_2
                document.getElementById('end_form_21_cib').addEventListener('click', () => {
                    this.load_page(1);
                });

                //Boton Atras Formulario_2_UTM
                document.getElementById('end_form_21_utm').addEventListener('click', () => {
                    this.load_page(6);
                });

                //Boton Siguiente Formulario_3
                var button_f3 = document.getElementById('end_form_34_cib');
                button_f3.addEventListener('click', () => {
                    this.selected_eq_f = [];
                    for (var i = 0; i < this.selected_eq.length; i++) {
                        var n = i.toString();
                        var id = 'check_btn_select_' + n;
                        if (document.getElementById(id).checked == true) {
                            this.selected_eq_f.push(this.selected_eq[i]);
                        }
                    }
                    if (this.selected_eq_f.length == 0) {
                        document.getElementById('least_one_list_select').style.display = 'block';
                    } else if (this.selected_eq_f.length == 1) {
                        document.getElementById('least_one_list_select').style.display = 'none';
                        this.load_page(4);
                    } else {
                        document.getElementById('least_one_list_select').style.display = 'none';
                        this.load_page(5);
                    }

                    //Obtener "Centroide" para el campo shape de Incidencias libres y sobre viario
                    for (var i = 0; i < this.selected_eq_f.length; i++) {
                        if (this.selected_eq_f[i].features["geometry"].declaredClass == 'esri.geometry.Polyline' && this.inc_grupo == '5') {
                            var numeroVerticeMedio = Math.round(this.selected_eq_f[i].features.geometry.paths[0].length / 2)
                            var centroide = this.selected_eq_f[i].features.geometry.paths[0][numeroVerticeMedio]
                            this.centroideViario.push(centroide)
                        }
                    }
                });

                //Boton Siguiente Formulario_3_UTM
                var button_f3_utm = document.getElementById('end_form_34_utm');
                button_f3_utm.addEventListener('click', () => {
                    this.selected_eq_f = [];
                    for (var i = 0; i < this.selected_eq.length; i++) {
                        var n = i.toString();
                        var id = 'check_btn_select_' + n;
                        if (document.getElementById(id).checked == true) {
                            this.selected_eq_f.push(this.selected_eq[i]);
                        }
                    }
                    if (this.selected_eq_f.length == 0) {
                        document.getElementById('least_one_list_select_utm').style.display = 'block';
                    } else if (this.selected_eq_f.length == 1) {
                        document.getElementById('least_one_list_select_utm').style.display = 'none';
                        this.load_page(9);
                    } else {
                        document.getElementById('least_one_list_select_utm').style.display = 'none';
                        this.load_page(10);
                    }

                    //Obtener "Centroide" para el campo shape de Incidencias libres y sobre viario
                    for (var i = 0; i < this.selected_eq_f.length; i++) {
                        if (this.selected_eq_f[i].features["geometry"].declaredClass == 'esri.geometry.Polyline' && this.inc_grupo == '5') {
                            var numeroVerticeMedio = Math.round(this.selected_eq_f[i].features.geometry.paths[0].length / 2)
                            var centroide = this.selected_eq_f[i].features.geometry.paths[0][numeroVerticeMedio]
                            this.centroideViario.push(centroide)
                        }
                    }
                });

                //Boton Atras Formulario_3
                document.getElementById('end_form_32_cib').addEventListener('click', () => {
                    this.load_page(2);
                    this.tablaPos_base = this.tablaPos_ini;
                    for (var i = 0; i < this.layerIncidencias.length; i++) {
                        var globalid_selected = "globalid IN (";
                        for (var j = 0; j < this.elementos_eq.length; j++) {
                            if (i == this.elementos_eq[j].index) {
                                globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
                            }
                        }
                        globalid_selected += "'')";
                        var id_eq;
                        for (var k = 0; k < this.layerEntidades.length; k++) {
                            if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                id_eq = this.layerEntidades[k].related_layers_id;
                            }
                        }
                        var query = new Query();
                        query.where = globalid_selected;
                        query.outFields = ["*"];
                        query.returnGeometry = true;
                        this.graphics_globalid = [];
                        for (var l = 0; l < this.Graphics_eq.graphics.length; l++) {
                            this.graphics_globalid.push(this.Graphics_eq.graphics[l].attributes.globalid);
                        }
                        this.Graphics_eq.clear();
                        this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                            for (var i = 0; i < result_select_graphics.length; i++) {
                                if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                    if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
                                        result_select_graphics[i].symbol = null;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setWidth(5);
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        result_select_graphics[i].symbol = line;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                } else {
                                    if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
                                        result_select_graphics[i].symbol = null;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        var marker = new SimpleMarkerSymbol();
                                        marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                        marker.setColor(new Color([0, 197, 255, 0.26]));
                                        marker.setAngle(0);
                                        marker.setOutline(line);
                                        result_select_graphics[i].symbol = marker;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                }
                            }
                            this.Graphics_eq.redraw();
                        }, (rollback_select_graphics) => {
                            this.Graphics_eq.clear();
                        });
                    }
                });

                //Boton Atras Formulario_3_UTM
                document.getElementById('end_form_32_utm').addEventListener('click', () => {
                    this.load_page(7);
                    this.tablaPos_base = this.tablaPos_ini;
                    for (var i = 0; i < this.layerIncidencias.length; i++) {
                        var globalid_selected = "globalid IN (";
                        for (var j = 0; j < this.elementos_eq.length; j++) {
                            if (i == this.elementos_eq[j].index) {
                                globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
                            }
                        }
                        globalid_selected += "'')";
                        var id_eq;
                        for (var k = 0; k < this.layerEntidades.length; k++) {
                            if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                id_eq = this.layerEntidades[k].related_layers_id;
                            }
                        }
                        var query = new Query();
                        query.where = globalid_selected;
                        query.outFields = ["*"];
                        query.returnGeometry = true;
                        this.graphics_globalid = [];
                        for (var l = 0; l < this.Graphics_eq.graphics.length; l++) {
                            this.graphics_globalid.push(this.Graphics_eq.graphics[l].attributes.globalid);
                        }
                        this.Graphics_eq.clear();
                        this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                            for (var i = 0; i < result_select_graphics.length; i++) {
                                if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                    if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
                                        result_select_graphics[i].symbol = null;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setWidth(5);
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        result_select_graphics[i].symbol = line;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                } else {
                                    if (this.graphics_globalid.includes(result_select_graphics[i].attributes.globalid) == false) {
                                        result_select_graphics[i].symbol = null;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        var marker = new SimpleMarkerSymbol();
                                        marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                        marker.setColor(new Color([0, 197, 255, 0.26]));
                                        marker.setAngle(0);
                                        marker.setOutline(line);
                                        result_select_graphics[i].symbol = marker;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                }
                            }
                            this.Graphics_eq.redraw();
                        }, (rollback_select_graphics) => {
                            this.Graphics_eq.clear();
                        });
                    }
                });

                //Boton Siguiente Formulario_4
                document.getElementById('end_form_43_cib').addEventListener('click', () => {
                    this.load_page(3);
                });

                //Boton Siguiente Formulario_4_UTM
                document.getElementById('end_form_43_utm').addEventListener('click', () => {
                    this.load_page(8);
                });

                //Boton Atras Formulario_4
                document.getElementById('end_form_45_cib').addEventListener('click', () => {
                    this.load_page(5);
                });

                //Boton Atras Formulario_4_UTM
                document.getElementById('end_form_45_utm').addEventListener('click', () => {
                    this.load_page(10);
                });

                //Boton Finalizar y Guardar Formulario 5
                var save_button = document.getElementById('end_form_55_cib');
                save_button.addEventListener('click', () => {
                    document.getElementById('btn_at_fi_5_cib').style.display = 'none';
                    this.create_new_incidence();
                });

                //Boton Finalizar y Guardar Formulario 5_UTM
                var save_button_utm = document.getElementById('end_form_55_utm');
                save_button_utm.addEventListener('click', () => {
                    document.getElementById('btn_at_fi_5_utm').style.display = 'none';
                    this.create_new_incidence_utm();
                });


                //Boton Atras Formulario_5
                document.getElementById('end_form_54_cib').addEventListener('click', () => {
                    if (this.selected_eq_f.length == 1) {
                        this.load_page(4);
                    } else {
                        this.load_page(3);
                    }
                });

                //Boton Atras Formulario_5_UTM
                document.getElementById('end_form_54_utm').addEventListener('click', () => {
                    if (this.selected_eq_f.length == 1) {
                        this.load_page(9);
                    } else {
                        this.load_page(8);
                    }
                });

                //Boton Volver al inicio y volver inicio UTM
                var end_btn = document.getElementById('end_cib');
                var end_btn_utm = document.getElementById('end_utm');
                end_btn.addEventListener('click', () => {
                    this.go_inicio();
                });
                end_btn_utm.addEventListener('click', () => {
                    this.go_inicio();
                });

                //Botones dialogo de salida
                document.getElementById('confirm_button_exit_cib').addEventListener('click', () => {
                    document.getElementById('dialogo_exit_cib').style.display = 'none';
                    this.go_inicio();
                });
                document.getElementById('cancel_button_exit_cib').addEventListener('click', function () {
                    document.getElementById('dialogo_exit_cib').style.display = 'none';
                });

            },

            loading: function () {
                domStyle.set(this.loadingGif, 'display', 'block');
            },

            finLoading: function () {
                domStyle.set(this.loadingGif, 'display', 'none');
            },

            botonDraw: function () {
                document.getElementById('geometry_oblig').style.display = 'none';
                this.Graphics_eq.clear();
                this.draw.activate('freehandpolygon');
            },

            geoIndicencia_utm: function (geometry) {
                this.geometry_inc = geometry;
                document.getElementById('geometry_oblig_utm').style.display = 'none';
                document.getElementById('select_region_utm').style.display = 'none';
                document.getElementById('least_one_list_utm').style.display = 'none';
                document.getElementById('glayer_novisible_utm').style.display = 'none';
                document.getElementById('entidad_list_utm').innerHTML = '';
                //document.getElementById('select_polygon_utm').innerHTML = "Cargando...";
                //Consultar las capas relacionadas con cada grupo
                var resultados = [];
                var consultas = [];
                this.elementos_eq = [];
                this.layerIncidencias = [];
                let coincide = false;
                for (var i = 0; i < this.layerEntidades.length; i++) {
                    var where = "1=1";
                    if (this.layerEntidades[i].related_layers_group_id == this.inc_grupo && this.map._layers[this.layerEntidades[i].related_layers_id].visible == true) {
                        coincide = true;
                        //Comprobar por el grupo seleccionado los filtros que se aplicarian
                        //Si el grupo seleccionado no tiene filtros adicionales
                        if (document.getElementById('grupo_inc').value == 2 || document.getElementById('grupo_inc').value == 4 || document.getElementById('grupo_inc').value == 20 || document.getElementById('grupo_inc').value == 21 || document.getElementById('grupo_inc').value == 22 || document.getElementById('grupo_inc').value == 23 || document.getElementById('grupo_inc').value == 24 || document.getElementById('grupo_inc').value == 25) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            }
                        } else if (document.getElementById('grupo_inc').value == 7) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            }
                        } else if (document.getElementById('grupo_inc').value == 9) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            }
                        } else if (document.getElementById('grupo_inc').value == 8) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            }
                        } else if (document.getElementById('grupo_inc').value == 31) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            }
                        } else if (document.getElementById('grupo_inc').value == 3) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            }
                        } else if (document.getElementById('grupo_inc').value == 5) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_via_id_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND tipo_via_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_via_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via != ")") {
                                var where = "tipo_via_id in" + this.valores_seleccionados_Via
                            }
                        }
                        var url_capa = this.layerEntidades[i].related_layers_url;
                        var query = new Query();
                        query.geometry = this.geometry_inc;
                        query.distance = this.config.utm_search.buffer_distance;
                        query.where = where;
                        query.units = this.config.utm_search.buffer_unit;
                        query.outFields = ["*"];
                        query.returnGeometry = true
                        this.layerIncidencias.push(this.layerEntidades[i].related_layers_title);
                        var query_elements = new QueryTask(url_capa);
                        consultas.push(query_elements.execute(query));
                    }
                }
                Promise.all(consultas).then(resultsBuffer => {
                    let consultas_distancias = [];
                    this.resBuffer = resultsBuffer;
                    for (let x = 0; x < resultsBuffer.length; x++) {
                        for (let y = 0; y < resultsBuffer[x].features.length; y++) {
                            let distParams = new DistanceParameters();
                            distParams.geometry1 = this.geometry_inc;
                            distParams.geometry2 = resultsBuffer[x].features[y].geometry;
                            distParams.geodesic = true;
                            let deferredDist = this.gsvc.distance(distParams);
                            consultas_distancias.push(deferredDist);
                        }
                    }
                    Promise.all(consultas_distancias).then(resultsDist => {
                        let long_total = 0;
                        for (let x = 0; x < this.resBuffer.length; x++) {
                            long_total += this.resBuffer[x].features.length;
                        }
                        if (long_total != resultsDist.length) {
                            console.error(`Las longitudes no coinciden. long_total: ${long_total}, resDist: ${resultsDist.length}`);
                            return;
                        }
                        let aux = 0;
                        let dist_obj = [];
                        for (let x = 0; x < this.resBuffer.length; x++) {
                            for (let y = 0; y < this.resBuffer[x].features.length; y++) {
                                this.resBuffer[x].features[y].distance = resultsDist[aux + y];
                                dist_obj.push({
                                    id: this.resBuffer[x].features[y].attributes.codigo,
                                    distance: this.resBuffer[x].features[y].distance,
                                    indices: {
                                        x: x,
                                        y: y
                                    }
                                });
                            }
                            aux += this.resBuffer[x].features.length;
                        }
                        dist_obj.sort((a, b) => {
                            if (a.distance < b.distance) {
                                return -1;
                            }
                            if (a.distance > b.distance) {
                                return 1;
                            }
                            return 0;
                        });
                        if (this.config.utm_search.buffer_max_results < dist_obj.length) {
                            dist_obj = dist_obj.slice(0, this.config.utm_search.buffer_max_results);
                        }
                        var all_items_eq = [];
                        let results = this.resBuffer;
                        let results_aux = [];
                        for (let z = 0; z < dist_obj.length; z++) {
                            let e = dist_obj[z];
                            if (results_aux[e.indices.x] == undefined) {
                                results_aux[e.indices.x] = {};
                                results_aux[e.indices.x].features = [];
                            }
                            results_aux[e.indices.x].features.push(results[e.indices.x].features[e.indices.y]);
                        }
                        for (let x = 0; x < results_aux.length; x++) {
                            results[x].features = results_aux[x].features;
                        }
                        //document.getElementById('select_polygon_utm').innerHTML = "Seleccionar en el Mapa";
                        if (results.length == 0) {
                            document.getElementById('entidad_list_utm').style.display = 'none';
                            document.getElementById('glayer_novisible_utm').style.display = 'block';
                        } else {
                            for (var i = 0; i < results.length; i++) {
                                resultados.push(results[i].features);
                            }
                            for (var i = 0; i < resultados.length; i++) {
                                for (var j = 0; j < resultados[i].length; j++) {
                                    all_items_eq.push(new this.Entidad(resultados[i][j], i));
                                }
                            }
                            this.elementos_eq = all_items_eq;
                            for (var i = 0; i < this.layerIncidencias.length; i++) {
                                var globalid_selected = "globalid IN (";
                                for (var j = 0; j < this.elementos_eq.length; j++) {
                                    if (i == this.elementos_eq[j].index) {
                                        globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
                                    }
                                }
                                globalid_selected += "'')";
                                var id_eq;
                                for (var k = 0; k < this.layerEntidades.length; k++) {
                                    if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                        id_eq = this.layerEntidades[k].related_layers_id;
                                    }
                                }
                                var query = new Query();
                                query.where = globalid_selected;
                                query.outFields = ["*"];
                                query.returnGeometry = true;
                                this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                                    for (var i = 0; i < result_select_graphics.length; i++) {
                                        if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                            var line = new SimpleLineSymbol();
                                            line.setWidth(5);
                                            line.setColor(new Color([0, 197, 255, 1]));
                                            result_select_graphics[i].symbol = line;
                                            this.Graphics_eq.add(result_select_graphics[i]);
                                            if (i == result_select_graphics.length - 1) {
                                                this.Graphics_eq.clear();
                                                for (var i = 0; i < result_select_graphics.length; i++) {
                                                    this.Graphics_eq.add(result_select_graphics[i]);
                                                }
                                            }
                                        } else {
                                            var line = new SimpleLineSymbol();
                                            line.setColor(new Color([0, 197, 255, 1]));
                                            var marker = new SimpleMarkerSymbol();
                                            marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                            marker.setColor(new Color([0, 197, 255, 0.26]));
                                            marker.setAngle(0);
                                            marker.setOutline(line);
                                            result_select_graphics[i].symbol = marker;
                                            this.Graphics_eq.add(result_select_graphics[i]);
                                        }
                                    }
                                    this.Graphics_eq.redraw();
                                    let puntoUtm_line = new SimpleLineSymbol();
                                    puntoUtm_line.setColor(new Color([255, 0, 0, 1]));
                                    let puntoUtm_marker = new SimpleMarkerSymbol();
                                    puntoUtm_marker.setSize(17);
                                    puntoUtm_marker.setOutline(puntoUtm_line);
                                    puntoUtm_marker.setStyle(SimpleMarkerSymbol.STYLE_CROSS);
                                    let puntoUtm_graphic = new Graphic(this.geometry_inc, puntoUtm_marker);
                                    this.glPuntoUTM.add(puntoUtm_graphic);
                                    this.glPuntoUTM.redraw();
                                    let graphics_extent = GraphicsUtils.graphicsExtent(this.Graphics_eq.graphics);
                                    this.map.setExtent(graphics_extent, true);
                                }, (rollback_select_graphics) => {
                                    this.Graphics_eq.clear();
                                });
                            }
                            this.load_table_utm();
                            this.finLoading();
                            this.load_page(7);
                        }
                    }).catch(err_dist => {
                        this.finLoading();
                        console.log(err_dist);
                        alert("Error en el cálculo de las distancias");
                        document.getElementById('entidad_list_utm').innerHTML = '<p class="t2_red">Se produjo un error en el cálculo de las distancias</p>';
                    });
                }).catch(error => {
                    // document.getElementById('select_polygon_utm').innerHTML = "Seleccionar en el Mapa";
                    this.finLoading();
                    console.log(error);
                    alert("Error en el cálculo del buffer");
                    document.getElementById('entidad_list_utm').innerHTML = '<p class="t2_red">Se produjo un error en el buffer</p>';
                });
                if (coincide === false) {
                    this.finLoading();
                    document.getElementById("grupo_novalido_utm").style.display = 'block';
                }
            },

            geoIndicencia: function (event) {
                this.draw.deactivate();
                this.geometry_inc = event.geometry;
                document.getElementById('geometry_oblig').style.display = 'none';
                document.getElementById('select_region').style.display = 'none';
                document.getElementById('least_one_list').style.display = 'none';
                document.getElementById('glayer_novisible').style.display = 'none';
                document.getElementById('entidad_list').innerHTML = '';
                document.getElementById('select_polygon').innerHTML = "Cargando...";
                //Consultar las capas relacionadas con cada grupo
                var resultados = [];
                var consultas = [];
                this.elementos_eq = [];
                this.layerIncidencias = [];
                let coincide = false;
                for (var i = 0; i < this.layerEntidades.length; i++) {
                    var where = "1=1";
                    if (this.layerEntidades[i].related_layers_group_id == this.inc_grupo && this.map._layers[this.layerEntidades[i].related_layers_id].visible == true) {
                        coincide = true;
                        //Comprobar por el grupo seleccionado los filtros que se aplicarian
                        //Si el grupo seleccionado no tiene filtros adicionales
                        if (document.getElementById('grupo_inc').value == 2 || document.getElementById('grupo_inc').value == 4 || document.getElementById('grupo_inc').value == 20 || document.getElementById('grupo_inc').value == 21 || document.getElementById('grupo_inc').value == 22 || document.getElementById('grupo_inc').value == 23 || document.getElementById('grupo_inc').value == 24 || document.getElementById('grupo_inc').value == 25) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            }
                        } else if (document.getElementById('grupo_inc').value == 7) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Arqueta != ")") {
                                var where = "arqueta_tipo_id in" + this.valores_seleccionados_Arqueta
                            }
                        } else if (document.getElementById('grupo_inc').value == 9) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Barrera != ")") {
                                var where = "barrera_tipo_id in" + this.valores_seleccionados_Barrera
                            }
                        } else if (document.getElementById('grupo_inc').value == 8) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Deposito != ")") {
                                var where = "deposito_tipo_id in" + this.valores_seleccionados_Deposito
                            }
                        } else if (document.getElementById('grupo_inc').value == 31) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Dotacion != ")") {
                                var where = "dotacion_tipo_id in" + this.valores_seleccionados_Dotacion
                            }
                        } else if (document.getElementById('grupo_inc').value == 3) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_InsRec != ")") {
                                var where = "tipo_instalacion_recreativa_id in" + this.valores_seleccionados_InsRec
                            }
                        } else if (document.getElementById('grupo_inc').value == 5) {
                            if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_via_id_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via == ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio != ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via != ")") {
                                var where = "territorio_id in" + this.valores_seleccionados_Territorio + " AND tipo_via_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via == ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor != ")" && this.valores_seleccionados_Via != ")") {
                                var where = "gestor_id in" + this.valores_seleccionados_Gestor + " AND tipo_via_id in" + this.valores_seleccionados_Via
                            } else if (this.valores_seleccionados_Territorio == ")" && this.valores_seleccionados_Gestor == ")" && this.valores_seleccionados_Via != ")") {
                                var where = "tipo_via_id in" + this.valores_seleccionados_Via
                            }
                        }
                        var url_capa = this.layerEntidades[i].related_layers_url;
                        var query = new Query();
                        query.geometry = this.geometry_inc;
                        query.where = where;
                        query.outFields = ["*"];
                        query.returnGeometry = true
                        this.layerIncidencias.push(this.layerEntidades[i].related_layers_title);
                        var query_elements = new QueryTask(url_capa);
                        this.loading();
                        consultas.push(query_elements.execute(query));
                    }
                }
                Promise.all(consultas).then(results => {
                    var all_items_eq = [];
                    document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
                    if (results.length == 0) {
                        document.getElementById('entidad_list').style.display = 'none';
                        document.getElementById('glayer_novisible').style.display = 'block';
                    } else {
                        for (var i = 0; i < results.length; i++) {
                            resultados.push(results[i].features);
                        }
                        for (var i = 0; i < resultados.length; i++) {
                            for (var j = 0; j < resultados[i].length; j++) {
                                all_items_eq.push(new this.Entidad(resultados[i][j], i));
                            }
                        }
                        this.elementos_eq = all_items_eq;
                        for (var i = 0; i < this.layerIncidencias.length; i++) {
                            var globalid_selected = "globalid IN (";
                            for (var j = 0; j < this.elementos_eq.length; j++) {
                                if (i == this.elementos_eq[j].index) {
                                    globalid_selected += "'" + this.elementos_eq[j].features.attributes.globalid + "',";
                                }
                            }
                            globalid_selected += "'')";
                            var id_eq;
                            for (var k = 0; k < this.layerEntidades.length; k++) {
                                if (this.layerIncidencias[i] == this.layerEntidades[k].related_layers_title) {
                                    id_eq = this.layerEntidades[k].related_layers_id;
                                }
                            }
                            var query = new Query();
                            query.where = globalid_selected;
                            query.outFields = ["*"];
                            query.returnGeometry = true;
                            this.map._layers[id_eq].selectFeatures(query, FeatureLayer.SELECTION_NEW, (result_select_graphics) => {
                                for (var i = 0; i < result_select_graphics.length; i++) {
                                    if (result_select_graphics[i].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                        var line = new SimpleLineSymbol();
                                        line.setWidth(5);
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        result_select_graphics[i].symbol = line;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                        if (i == result_select_graphics.length - 1) {
                                            this.Graphics_eq.clear();
                                            for (var i = 0; i < result_select_graphics.length; i++) {
                                                this.Graphics_eq.add(result_select_graphics[i]);
                                            }
                                        }
                                    } else {
                                        var line = new SimpleLineSymbol();
                                        line.setColor(new Color([0, 197, 255, 1]));
                                        var marker = new SimpleMarkerSymbol();
                                        marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                        marker.setColor(new Color([0, 197, 255, 0.26]));
                                        marker.setAngle(0);
                                        marker.setOutline(line);
                                        result_select_graphics[i].symbol = marker;
                                        this.Graphics_eq.add(result_select_graphics[i]);
                                    }
                                }
                                this.Graphics_eq.redraw();
                            }, (rollback_select_graphics) => {
                                this.Graphics_eq.clear();
                            });
                        }
                        this.load_table();
                        this.finLoading();
                    }
                }).catch(error => {
                    this.finLoading();
                    document.getElementById('select_polygon').innerHTML = "Seleccionar en el Mapa";
                    document.getElementById('entidad_list').innerHTML = '<p class="t2_red">Se produjo un error, dibuje la zona afectada nuevamente</p>';
                });
                if (coincide === false) {
                    this.finLoading();
                    document.getElementById('grupo_novalido').style.display = 'block';
                }
            },

            load_table: function () {
                this.tablaPos_ini = [];
                if (this.elementos_eq.length == 0) {
                    document.getElementById('entidad_list').style.display = 'none';
                    document.getElementById('select_region').style.display = 'block';
                } else {
                    document.getElementById('entidad_list').style.display = 'block';
                    var ul = DomConstruct.toDom("<ul class='list-group'>");
                    DomConstruct.place(ul, 'entidad_list', 'last');
                    for (var i = 0; i < this.elementos_eq.length; i++) {
                        if (this.elementos_eq[i].features.attributes.codigo != undefined && this.elementos_eq[i].features.attributes.nombre != undefined) {
                            var territorio_tab = '';
                            for (var j = 0; j < this.TerritorioValue.length; j++) {
                                if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                    territorio_tab = this.Territorio[j];
                                }
                            }
                            var li = DomConstruct.toDom("<li class='list-group-item'>");
                            var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                            on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                            var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                            on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                            DomConstruct.place(input, li, 'last');
                            DomConstruct.place(span, li, 'last');
                            DomConstruct.place(li, ul, 'last');
                            this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                        } else {
                            if (this.elementos_eq[i].features.attributes.source != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.elementos_eq[i].features.attributes.globalid + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            } else if (this.elementos_eq[i].features.attributes.nombre != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            } else {
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.elementos_eq[i].features.attributes.globalid + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            }
                        }
                    }
                    this.tablaPos_base = this.tablaPos_ini;
                }
            },

            load_table_utm: function () {
                this.tablaPos_ini = [];
                let tabla = document.getElementById('entidad_list_utm');
                if (tabla.firstChild !== null && tabla.firstChild.children.length > 0) {
                    tabla.firstChild.innerHTML = '';
                }
                if (this.elementos_eq.length == 0) {
                    document.getElementById('entidad_list_utm').style.display = 'none';
                    document.getElementById('select_region_utm').style.display = 'block';
                } else {
                    document.getElementById('entidad_list_utm').style.display = 'block';
                    var ul = DomConstruct.toDom("<ul class='list-group'>");
                    DomConstruct.place(ul, 'entidad_list_utm', 'last');
                    for (var i = 0; i < this.elementos_eq.length; i++) {
                        if (this.elementos_eq[i].features.attributes.codigo != undefined && this.elementos_eq[i].features.attributes.nombre != undefined) {
                            var territorio_tab = '';
                            for (var j = 0; j < this.TerritorioValue.length; j++) {
                                if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                    territorio_tab = this.Territorio[j];
                                }
                            }
                            var li = DomConstruct.toDom("<li class='list-group-item'>");
                            var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                            on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                            var span = DomConstruct.toDom("<span class='t2_tab'>" + " (" + Math.round(this.elementos_eq[i].features.distance) + "m) " + this.elementos_eq[i].features.attributes.codigo + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                            on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                            DomConstruct.place(input, li, 'last');
                            DomConstruct.place(span, li, 'last');
                            DomConstruct.place(li, ul, 'last');
                            this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                        } else {
                            if (this.elementos_eq[i].features.attributes.source != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + `(${Math.round(this.elementos_eq[i].features.distance)}m)` + " Tramo: " + this.elementos_eq[i].features.attributes.globalid + " | " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            } else if (this.elementos_eq[i].features.attributes.nombre != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.elementos_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + `(${Math.round(this.elementos_eq[i].features.distance)}m)` + " " + this.elementos_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            } else {
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + `(${Math.round(this.elementos_eq[i].features.distance)}m)` + " " + this.elementos_eq[i].features.attributes.globalid + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.elementos_eq[i].features.attributes.globalid, i, "check_btn_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_ini[i] = new this.Tabla_localizador(i, this.elementos_eq[i].features.attributes.globalid);
                            }
                        }
                    }
                    this.tablaPos_base = this.tablaPos_ini;
                }
            },

            load_table_selected: function () {
                this.tablaPos_fin = [];
                document.getElementById('entidad_list_select').innerHTML = '';
                if (this.selected_eq.length == 0) {
                    document.getElementById('entidad_list_select').style.display = 'none';
                    document.getElementById('no_list').style.display = 'block';
                } else {
                    document.getElementById('entidad_list_select').style.display = 'block';
                    var ul = DomConstruct.toDom("<ul class='list-group'>");
                    DomConstruct.place(ul, 'entidad_list_select', 'last');
                    for (var i = 0; i < this.selected_eq.length; i++) {
                        if (this.selected_eq[i].features.attributes.codigo != undefined && this.selected_eq[i].features.attributes.nombre != undefined) {
                            var territorio_tab = '';
                            for (var j = 0; j < this.TerritorioValue.length; j++) {
                                if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                    territorio_tab = this.Territorio[j];
                                }
                            }
                            var li = DomConstruct.toDom("<li class='list-group-item'>");
                            var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                            on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                            var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.codigo + " | " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                            on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                            DomConstruct.place(input, li, 'last');
                            DomConstruct.place(span, li, 'last');
                            DomConstruct.place(li, ul, 'last');
                            this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                        } else {
                            if (this.selected_eq[i].features.attributes.source != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.selected_eq[i].features.attributes.globalid + " | " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            } else if (this.selected_eq[i].features.attributes.nombre != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            } else {
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.globalid + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            }
                        }
                    }
                    this.tablaPos_base = this.tablaPos_fin;
                }
            },

            load_table_selected_utm: function () {
                this.tablaPos_fin = [];
                document.getElementById('entidad_list_select_utm').innerHTML = '';
                if (this.selected_eq.length == 0) {
                    document.getElementById('entidad_list_select_utm').style.display = 'none';
                    document.getElementById('no_list_utm').style.display = 'block';
                } else {
                    document.getElementById('entidad_list_select_utm').style.display = 'block';
                    var ul = DomConstruct.toDom("<ul class='list-group'>");
                    DomConstruct.place(ul, 'entidad_list_select_utm', 'last');
                    for (var i = 0; i < this.selected_eq.length; i++) {
                        if (this.selected_eq[i].features.attributes.codigo != undefined && this.selected_eq[i].features.attributes.nombre != undefined) {
                            var territorio_tab = '';
                            for (var j = 0; j < this.TerritorioValue.length; j++) {
                                if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                    territorio_tab = this.Territorio[j];
                                }
                            }
                            var li = DomConstruct.toDom("<li class='list-group-item'>");
                            var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                            on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                            var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.codigo + " | " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                            on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                            DomConstruct.place(input, li, 'last');
                            DomConstruct.place(span, li, 'last');
                            DomConstruct.place(li, ul, 'last');
                            this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                        } else {
                            if (this.selected_eq[i].features.attributes.source != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " Tramo: " + this.selected_eq[i].features.attributes.globalid + " | " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            } else if (this.selected_eq[i].features.attributes.nombre != undefined) {
                                var territorio_tab = '';
                                for (var j = 0; j < this.TerritorioValue.length; j++) {
                                    if (this.TerritorioValue[j] == this.selected_eq[i].features.attributes.territorio_id) {
                                        territorio_tab = this.Territorio[j];
                                    }
                                }
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.nombre + " | " + territorio_tab + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            } else {
                                var li = DomConstruct.toDom("<li class='list-group-item'>");
                                var input = DomConstruct.toDom("<input type='checkbox' checked='true' id='check_btn_select_" + i + "'>");
                                on(input, 'click', lang.hitch(this, this.comprobar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                var span = DomConstruct.toDom("<span class='t2_tab'>" + " " + this.selected_eq[i].features.attributes.globalid + "</span>");
                                on(span, 'click', lang.hitch(this, this.resaltar_graphics(this.selected_eq[i].features.attributes.globalid, i, "check_btn_select_")));
                                DomConstruct.place(input, li, 'last');
                                DomConstruct.place(span, li, 'last');
                                DomConstruct.place(li, ul, 'last');
                                this.tablaPos_fin[i] = new this.Tabla_localizador(i, this.selected_eq[i].features.attributes.globalid);
                            }
                        }
                    }
                    this.tablaPos_base = this.tablaPos_fin;
                }
            },

            resaltar_graphics: function (globalid, id_button, tipo) {
                return function () {
                    var id = tipo + id_button;
                    if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
                        for (var i = 0; i < this.Graphics_eq.graphics.length; i++) {
                            if (globalid == this.Graphics_eq.graphics[i].attributes.globalid) {
                                if (this.Graphics_eq.graphics[0].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                    var line = new SimpleLineSymbol();
                                    line.setWidth(5);
                                    line.setColor(new Color([230, 0, 0, 1]));
                                    this.Graphics_eq.graphics[i].setSymbol(line);
                                } else {
                                    var line = new SimpleLineSymbol();
                                    line.setColor(new Color([230, 0, 0, 1]));
                                    var marker = new SimpleMarkerSymbol();
                                    marker.setOutline(line);
                                    marker.setColor(new Color([230, 0, 0, 0.25]));
                                    marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                    this.Graphics_eq.graphics[i].setSymbol(marker);
                                }
                            } else {
                                for (var j = 0; j < this.tablaPos_base.length; j++) {
                                    var id = tipo + this.tablaPos_base[j].posicion;
                                    if (document.getElementById(id).checked == true && this.tablaPos_base[j].globalid == this.Graphics_eq.graphics[i].attributes.globalid)
                                        if (this.Graphics_eq.graphics[0].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                            var line = new SimpleLineSymbol();
                                            line.setWidth(5);
                                            line.setColor(new Color([0, 197, 255, 1]));
                                            this.Graphics_eq.graphics[i].setSymbol(line);
                                        } else {
                                            var line = new SimpleLineSymbol();
                                            line.setColor(new Color([0, 197, 255, 1]));
                                            var marker = new SimpleMarkerSymbol();
                                            marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                            marker.setColor(new Color([0, 197, 255, 0.26]));
                                            marker.setOutline(line);
                                            this.Graphics_eq.graphics[i].setSymbol(marker);
                                        }
                                }
                            }
                        }
                    }
                }
            },

            comprobar_graphics: function (globalid, id_button, tipo) {

                return function () {
                    var id = tipo + id_button;
                    if (document.getElementById(id) != null && document.getElementById(id).checked == true) {
                        for (var i = 0; i < this.Graphics_eq.graphics.length; i++) {
                            if (globalid == this.Graphics_eq.graphics[i].attributes.globalid) {
                                if (this.Graphics_eq.graphics[0].geometry["declaredClass"] == 'esri.geometry.Polyline') {
                                    var line = new SimpleLineSymbol();
                                    line.setWidth(5);
                                    line.setColor(new Color([0, 197, 255, 1]));
                                    this.Graphics_eq.graphics[i].setSymbol(line);
                                } else {
                                    var line = new SimpleLineSymbol();
                                    line.setColor(new Color([0, 197, 255, 1]));
                                    var marker = new SimpleMarkerSymbol();
                                    marker.setStyle(SimpleMarkerSymbol.STYLE_SQUARE);
                                    marker.setColor(new Color([0, 197, 255, 0.26]));
                                    marker.setOutline(line);
                                    this.Graphics_eq.graphics[i].setSymbol(marker);
                                }
                            }
                        }
                    } else {
                        for (var i = 0; i < this.Graphics_eq.graphics.length; i++) {
                            if (globalid == this.Graphics_eq.graphics[i].attributes.globalid) {
                                this.Graphics_eq.graphics[i].setSymbol(null)
                            }
                        }
                    }
                }
            },

            go_inicio: function () {
                this.reseteo();
                this.load_page(0);
            },

            reseteo: function () {
                this.finLoading();
                this.elementos_eq = [];
                this.centroideViario = [];
                this.selected_eq = [];
                this.selected_eq_f = [];
                this.geometry_inc = '';
                this.Graphics_eq.clear();
                document.getElementById('grupo_inc').value = "";
                document.getElementById('motivo_inc').innerHTML = '';
                document.getElementById('prioridad_inc').value = '0';
                document.getElementById('description_inc').value = '';
                document.getElementById('accion_inc').value = '1';
                document.getElementById('gestor_inc').value = '1';
                document.getElementById('notificacion_inc').value = '0';
                document.getElementById('territoriales_inc').value = '0';
                document.getElementById('cecopin_inc').value = '0';
                document.getElementById('restricciones_inc').value = '0';
                document.getElementById('res_finicio').value = '';
                document.getElementById('res_ffin').value = '';
                document.getElementById('res_enlace').value = '';
                document.getElementById('entidad_list').innerHTML = '';
                document.getElementById('end_cib').style.display = 'none';
                document.getElementById('btn_at_fi_5_cib').style.display = 'flex';
                document.getElementById('btn_dialogo_cib').style.display = 'flex';
                document.getElementById('loading_msg').innerHTML = '';
                document.getElementById('grupo_oblig').style.display = 'none';
                document.getElementById('descripcion_oblig').style.display = 'none';
                document.getElementById("utParrafo").style.display = "none";
                document.getElementById("territoriales_inc").style.display = "none";
                document.getElementById("cecopinParrafo").style.display = "none";
                document.getElementById("cecopin_inc").style.display = "none";
                document.getElementById("titulo_finicio").style.display = "none";
                document.getElementById("res_finicio").style.display = "none";
                document.getElementById("titulo_ffin").style.display = "none";
                document.getElementById("res_ffin").style.display = "none";
                document.getElementById("enlaceResParrafo").style.display = "none";
                document.getElementById("res_enlace").style.display = "none";
                document.getElementById("MultiselecAfectaItine").style.display = "none";
                document.getElementById('grupo_novalido').style.display = 'none';

                document.getElementById('input_utm_x').value = "";
                document.getElementById('input_utm_y').value = "";
                document.getElementById('grupo_inc_utm').value = "";
                document.getElementById('motivo_inc_utm').innerHTML = '';
                document.getElementById('prioridad_inc_utm').value = '0';
                document.getElementById('description_inc_utm').value = '';
                document.getElementById('accion_inc_utm').value = '1';
                document.getElementById('gestor_inc_utm').value = '1';
                document.getElementById('notificacion_inc_utm').value = '0';
                document.getElementById('territoriales_inc_utm').value = '0';
                document.getElementById('cecopin_inc_utm').value = '0';
                document.getElementById('restricciones_inc_utm').value = '0';
                document.getElementById('res_finicio_utm').value = '';
                document.getElementById('res_ffin_utm').value = '';
                document.getElementById('res_enlace_utm').value = '';
                document.getElementById('entidad_list_utm').innerHTML = '';
                document.getElementById('end_utm').style.display = 'none';
                document.getElementById('btn_at_fi_5_utm').style.display = 'flex';
                document.getElementById('loading_msg_utm').innerHTML = '';
                document.getElementById('grupo_oblig_utm').style.display = 'none';
                document.getElementById('descripcion_oblig_utm').style.display = 'none';
                document.getElementById("utParrafo_utm").style.display = "none";
                document.getElementById("territoriales_inc_utm").style.display = "none";
                document.getElementById("cecopinParrafo_utm").style.display = "none";
                document.getElementById("cecopin_inc_utm").style.display = "none";
                document.getElementById("titulo_finicio_utm").style.display = "none";
                document.getElementById("res_finicio_utm").style.display = "none";
                document.getElementById("titulo_ffin_utm").style.display = "none";
                document.getElementById("res_ffin_utm").style.display = "none";
                document.getElementById("enlaceResParrafo_utm").style.display = "none";
                document.getElementById("res_enlace_utm").style.display = "none";
                document.getElementById("MultiselecAfectaItine_utm").style.display = "none";
                document.getElementById('grupo_novalido_utm').style.display = 'none';

                this._uploadField.value = '';
                //Resetear mutiselect
                //Filtro territorio
                document.getElementById("Contador_ter").innerHTML = 0
                document.getElementById("FiltroTextoTerritorio").value = "";
                this.valores_seleccionados_Territorio = ")";
                document.getElementById("jimu_dijit_Popup_territorio").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableTerritorio")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Territorio.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_ter")
                    if (seleccionable.className == "checkInput checkbox checked Territorio") {
                        seleccionable.className = "checkInput checkbox Territorio";
                    }
                }
                //Filtro Gestor
                document.getElementById("Contador_ges").innerHTML = 0
                document.getElementById("FiltroTextoGestor").value = "";
                this.valores_seleccionados_Gestor = ")";
                document.getElementById("jimu_dijit_Popup_Gestor").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableGestor")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Gestor.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_ges")
                    if (seleccionable.className == "checkInput checkbox checked Gestor") {
                        seleccionable.className = "checkInput checkbox Gestor";
                    }
                }
                //Filtro Dotacion
                document.getElementById("Contador_dot").innerHTML = 0
                document.getElementById("FiltroTextoDotacion").value = "";
                this.valores_seleccionados_Dotacion = ")";
                document.getElementById("jimu_dijit_Popup_Dotacion").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableDotacion")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Dotacion.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_dot")
                    if (seleccionable.className == "checkInput checkbox checked Dotacion") {
                        seleccionable.className = "checkInput checkbox Dotacion";
                    }
                }
                //Filtro Instalacion recreativa
                document.getElementById("Contador_insrec").innerHTML = 0
                document.getElementById("FiltroTextoInsRec").value = "";
                this.valores_seleccionados_InsRec = ")";
                document.getElementById("jimu_dijit_Popup_InsRec").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableInsRec")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.InsRec.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_insrec")
                    if (seleccionable.className == "checkInput checkbox checked InsRec") {
                        seleccionable.className = "checkInput checkbox InsRec";
                    }
                }
                //Filtro Arqueta
                document.getElementById("Contador_arq").innerHTML = 0
                document.getElementById("FiltroTextoarq").value = "";
                this.valores_seleccionados_Arqueta = ")";
                document.getElementById("jimu_dijit_Popup_Arqueta").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableArqueta")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Arqueta.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_arq")
                    if (seleccionable.className == "checkInput checkbox checked Arqueta") {
                        seleccionable.className = "checkInput checkbox Arqueta";
                    }
                }
                //Filtro Deposito
                document.getElementById("Contador_dep").innerHTML = 0
                document.getElementById("FiltroTextodep").value = "";
                this.valores_seleccionados_Deposito = ")";
                document.getElementById("jimu_dijit_Popup_Deposito").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableDeposito")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Deposito.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_dep")
                    if (seleccionable.className == "checkInput checkbox checked Deposito") {
                        seleccionable.className = "checkInput checkbox Deposito";
                    }
                }
                //Filtro Barrera
                document.getElementById("Contador_bar").innerHTML = 0
                document.getElementById("FiltroTextobar").value = "";
                this.valores_seleccionados_Barrera = ")";
                document.getElementById("jimu_dijit_Popup_Barrera").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableBarrera")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Barrera.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_bar")
                    if (seleccionable.className == "checkInput checkbox checked Barrera") {
                        seleccionable.className = "checkInput checkbox Barrera";
                    }
                }
                //Filtro Via
                document.getElementById("Contador_via").innerHTML = 0
                document.getElementById("FiltroTextovia").value = "";
                this.valores_seleccionados_Via = ")";
                document.getElementById("jimu_dijit_Popup_Via").style.display = 'none';
                var filtro_texto = document.getElementById("desplegableVia")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.Via.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_via")
                    if (seleccionable.className == "checkInput checkbox checked Via") {
                        seleccionable.className = "checkInput checkbox Via";
                    }
                }

                //Filtro Itinerario afectado
                document.getElementById("Contador_iteafec").innerHTML = 0
                document.getElementById("FiltroTextoIteAfec").value = "";
                this.valores_seleccionados_ItinerarioAfectado = [];
                document.getElementById("jimu_dijit_Popup_ItinerarioAfectado").style.display = "none";
                var filtro_texto = document.getElementById("desplegableItinerarioAfectado")
                for (var i = 1; i < filtro_texto.childNodes.length; i++) {
                    filtro_texto.childNodes[i].style.display = "block";
                }
                document.getElementById("Contador_iteafec_utm").innerHTML = 0
                document.getElementById("FiltroTextoIteAfec_utm").value = "";
                document.getElementById("jimu_dijit_Popup_ItinerarioAfectado_utm").style.display = "none";
                var filtro_texto_utm = document.getElementById("desplegableItinerarioAfectado_utm")
                for (var i = 1; i < filtro_texto_utm.childNodes.length; i++) {
                    filtro_texto_utm.childNodes[i].style.display = "block";
                }
                for (var i = 0; i < this.ItinerarioAfectado.length; i++) {
                    var seleccionable = document.getElementById("seleccionable_" + i + "_iteafec")
                    var seleccionable_utm = document.getElementById("seleccionable_" + i + "_iteafec_utm")
                    if (seleccionable.className == "checkInput checkbox checked ItinerarioAfectado") {
                        seleccionable.className = "checkInput checkbox ItinerarioAfectado";
                    }
                    if (seleccionable_utm.className == "checkInput checkbox checked ItinerarioAfectado") {
                        seleccionable_utm.className = "checkInput checkbox ItinerarioAfectado";
                    }
                }
            },

            load_page: function (num) {
                document.getElementById('inicio_cib').style.display = 'none';
                document.getElementById('form_1_cib').style.display = 'none';
                document.getElementById('form_2_cib').style.display = 'none';
                document.getElementById('form_3_cib').style.display = 'none';
                document.getElementById('form_4_cib').style.display = 'none';
                document.getElementById('form_5_cib').style.display = 'none';

                document.getElementById('form_1_utm').style.display = 'none';
                document.getElementById('form_2_utm').style.display = 'none';
                document.getElementById('form_3_utm').style.display = 'none';
                document.getElementById('form_4_utm').style.display = 'none';
                document.getElementById('form_5_utm').style.display = 'none';

                document.getElementById('foot_1_cib').className = 'dot';
                document.getElementById('foot_2_cib').className = 'dot';
                document.getElementById('foot_3_cib').className = 'dot';
                document.getElementById('foot_4_cib').className = 'dot';
                document.getElementById('foot_5_cib').className = 'dot';
                if (num > 0 && num <= 10) {
                    document.getElementById('footer_cib').style.display = 'block';
                } else {
                    document.getElementById('footer_cib').style.display = 'none';
                    if (num == 0) {
                        document.getElementById('inicio_cib').style.display = 'block';
                        this.glPuntoUTM.clear();
                    }
                }
                switch (num) {
                    case 1:
                        document.getElementById('foot_1_cib').className = 'dot_check';
                        document.getElementById('form_1_cib').style.display = 'block';
                        break;
                    case 2:
                        document.getElementById('foot_2_cib').className = 'dot_check';
                        document.getElementById('form_2_cib').style.display = 'block';
                        break;
                    case 3:
                        document.getElementById('foot_3_cib').className = 'dot_check';
                        document.getElementById('form_3_cib').style.display = 'block';
                        break;
                    case 4:
                        document.getElementById('foot_4_cib').className = 'dot_check';
                        document.getElementById('form_4_cib').style.display = 'block';
                        break;
                    case 5:
                        document.getElementById('foot_5_cib').className = 'dot_check';
                        document.getElementById('form_5_cib').style.display = 'block';
                        break;
                    case 6:
                        document.getElementById('foot_1_cib').className = 'dot_check';
                        document.getElementById('form_1_utm').style.display = 'block';
                        this.glPuntoUTM.clear();
                        break;
                    case 7:
                        document.getElementById('foot_2_cib').className = 'dot_check';
                        document.getElementById('form_2_utm').style.display = 'block';
                        break;
                    case 8:
                        document.getElementById('foot_3_cib').className = 'dot_check';
                        document.getElementById('form_3_utm').style.display = 'block';
                        break;
                    case 9:
                        document.getElementById('foot_4_cib').className = 'dot_check';
                        document.getElementById('form_4_utm').style.display = 'block';
                        break;
                    case 10:
                        document.getElementById('foot_5_cib').className = 'dot_check';
                        document.getElementById('form_5_utm').style.display = 'block';
                        break;
                }
            },

            // DESPLEGAR DROPBOX MULTISELECT
            _desplegarDropbox: function (event) {
                if (event.currentTarget.className == 'MultiselectTerritorio') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_territorio");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectGestor') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Gestor");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectDotacion') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Dotacion");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectInsRec') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_InsRec");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectArqueta') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Arqueta");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectDeposito') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Deposito");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectBarrera') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Barrera");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectVia') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_Via");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectItinerarioAfectado') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_ItinerarioAfectado");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                } else if (event.currentTarget.className == 'MultiselectItinerarioAfectado_utm') {
                    var panel_desplegar = document.getElementById("jimu_dijit_Popup_ItinerarioAfectado_utm");
                    if (panel_desplegar.style.display == 'none') {
                        panel_desplegar.style.display = 'block';
                    } else {
                        panel_desplegar.style.display = 'none';
                    }
                }
            },

            //FILTRAR MULTISELECT
            _filtroMultiselect: function (event) {
                var filtro = document.getElementById(event.target.id).value
                for (var i = 0; i < event.composedPath()[4].childNodes[3].children[0].children.length; i++) {
                    //RESETEO
                    document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "block"
                    if (document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].childNodes[1].innerText.toLowerCase().includes(filtro.toLowerCase())) {
                        console.log("Empieza por: " + filtro)
                        document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "block"
                    } else {
                        console.log("No empieza por: " + filtro)
                        document.getElementById(event.composedPath()[4].childNodes[3].children[0].id).children[i].style.display = "none"
                    }
                }
            },

            _checking: function (event) {
                if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Gestor" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Gestor") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Gestor") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Gestor";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Gestor";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_ges").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Gestor")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_ges"
                        if (document.getElementById(id).className == "checkInput checkbox checked Gestor") {
                            conteo += 1
                            document.getElementById("Contador_ges").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Gestor")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_ges"
                        if (document.getElementById(id).className == "checkInput checkbox checked Gestor") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Gestor = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Territorio" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Territorio") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Territorio") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Territorio";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Territorio";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_ter").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Territorio")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_ter"
                        if (document.getElementById(id).className == "checkInput checkbox checked Territorio") {
                            conteo += 1
                            document.getElementById("Contador_ter").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Territorio")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_ter"
                        if (document.getElementById(id).className == "checkInput checkbox checked Territorio") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Territorio = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Dotacion" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Dotacion") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Dotacion") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Dotacion";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Dotacion";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_dot").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Dotacion")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_dot"
                        if (document.getElementById(id).className == "checkInput checkbox checked Dotacion") {
                            conteo += 1
                            document.getElementById("Contador_dot").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Dotacion")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_dot"
                        if (document.getElementById(id).className == "checkInput checkbox checked Dotacion") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Dotacion = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox InsRec" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked InsRec") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox InsRec") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked InsRec";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox InsRec";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_insrec").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list InsRec")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_insrec"
                        if (document.getElementById(id).className == "checkInput checkbox checked InsRec") {
                            conteo += 1
                            document.getElementById("Contador_insrec").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list InsRec")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_insrec"
                        if (document.getElementById(id).className == "checkInput checkbox checked InsRec") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_InsRec = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Arqueta" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Arqueta") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Arqueta") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Arqueta";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Arqueta";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_arq").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Arqueta")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_arq"
                        if (document.getElementById(id).className == "checkInput checkbox checked Arqueta") {
                            conteo += 1
                            document.getElementById("Contador_arq").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Arqueta")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_arq"
                        if (document.getElementById(id).className == "checkInput checkbox checked Arqueta") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Arqueta = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Deposito" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Deposito") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Deposito") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Deposito";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Deposito";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_dep").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Deposito")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_dep"
                        if (document.getElementById(id).className == "checkInput checkbox checked Deposito") {
                            conteo += 1
                            document.getElementById("Contador_dep").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Deposito")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_dep"
                        if (document.getElementById(id).className == "checkInput checkbox checked Deposito") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Deposito = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Barrera" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Barrera") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Barrera") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Barrera";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Barrera";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_bar").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Barrera")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_bar"
                        if (document.getElementById(id).className == "checkInput checkbox checked Barrera") {
                            conteo += 1
                            document.getElementById("Contador_bar").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Barrera")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_bar"
                        if (document.getElementById(id).className == "checkInput checkbox checked Barrera") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Barrera = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Via" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked Via") {
                    this.Graphics_eq.clear();
                    document.getElementById('entidad_list').innerHTML = '';
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox Via") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked Via";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox Via";
                    }

                    var conteo = 0;
                    document.getElementById("Contador_via").innerHTML = conteo
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Via")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_via"
                        if (document.getElementById(id).className == "checkInput checkbox checked Via") {
                            conteo += 1
                            document.getElementById("Contador_via").innerHTML = conteo
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    var valores_seleccionados = "(";
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list Via")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_via"
                        if (document.getElementById(id).className == "checkInput checkbox checked Via") {
                            valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                        }
                    }
                    var sin_coma = valores_seleccionados.substring(0, valores_seleccionados.length - 1);
                    valores_seleccionados = sin_coma + ")";
                    this.valores_seleccionados_Via = valores_seleccionados;
                    console.log(valores_seleccionados);
                } else if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox ItinerarioAfectado" || document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox checked ItinerarioAfectado") {
                    if (document.getElementById(event.composedPath()[0].id).className == "checkInput checkbox ItinerarioAfectado") {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox checked ItinerarioAfectado";
                    } else {
                        document.getElementById(event.composedPath()[0].id).className = "checkInput checkbox ItinerarioAfectado";
                    }

                    var conteo = 0;
                    var conteo_utm = 0;
                    document.getElementById("Contador_iteafec").innerHTML = conteo
                    document.getElementById("Contador_iteafec_utm").innerHTML = conteo_utm
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list ItinerarioAfectado")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_iteafec"
                        var id_utm = "seleccionable_" + i + "_iteafec_utm"
                        if (document.getElementById(id).className == "checkInput checkbox checked ItinerarioAfectado") {
                            conteo += 1
                            document.getElementById("Contador_iteafec").innerHTML = conteo
                        }
                        if (document.getElementById(id_utm).className == "checkInput checkbox checked ItinerarioAfectado") {
                            conteo_utm += 1
                            document.getElementById("Contador_iteafec_utm").innerHTML = conteo_utm
                        }
                    }

                    //OBTENER VALORES MULTISELECT
                    this.valores_seleccionados_ItinerarioAfectado = [];
                    for (var i = 0; i < document.getElementsByClassName("jimu-multiple-items-list ItinerarioAfectado")[0].childElementCount; i++) {
                        var id = "seleccionable_" + i + "_iteafec"
                        var id_utm = "seleccionable_" + i + "_iteafec_utm"
                        if (document.getElementById(id).className == "checkInput checkbox checked ItinerarioAfectado") {
                            //valores_seleccionados += "'" + document.getElementById(id).getAttribute("data-value") + "'" + ","
                            this.valores_seleccionados_ItinerarioAfectado.push(document.getElementById(id).getAttribute("data-value"))
                        }
                        if (document.getElementById(id_utm).className == "checkInput checkbox checked ItinerarioAfectado") {
                            this.valores_seleccionados_ItinerarioAfectado.push(document.getElementById(id_utm).getAttribute("data-value"))
                        }
                    }
                    console.log(this.valores_seleccionados_ItinerarioAfectado);
                }
            },


            create_new_incidence: function () {
                this.loading();
                document.getElementById('loading_msg').innerHTML = "Guardando Incidencias...";
                var grupoIncidencia = document.getElementById("grupo_inc").value;
                var codigo_uuid = this.uuid();

                if (grupoIncidencia == '5') {
                    var puntos_Total = [];

                    for (var i = 0; i < this.selected_eq_f.length; i++) {
                        var puntos_i = new Point();
                        puntos_i.spatialReference = this.map.spatialReference
                        puntos_i.x = this.centroideViario[i][0]
                        puntos_i.y = this.centroideViario[i][1]
                        puntos_Total.push(puntos_i)
                    }
                }

                var tabla_incidencia = new FeatureLayer(this.url_incidencia);
                var ToAdd = []
                var contador = 0;
                if (this.valores_seleccionados_ItinerarioAfectado.length == 0) {
                    this.valores_seleccionados_ItinerarioAfectado = [0];
                }
                for (var i = 0; i < this.selected_eq_f.length; i++) { //Recorrer los elementos seleccionados
                    for (var j = 0; j < this.valores_seleccionados_ItinerarioAfectado.length; j++) {
                        ToAdd[contador] = new Graphic();
                        ToAdd[contador].attributes = {
                            objectid: null, globalid: null, codigo: null, grupo_incidencia_id: null, subgrupo_incidencia_id: null, nombre: null, incidencia_prioridad_id: null,
                            descripcion: null, incidencia_tipo_accion_id: null, estado_incidencia_id: null, notificacion: null, destinatario_ut: null, destinatario_st: null, notificar_cecopin: null,
                            restriccion: null, restriccion_inicio: null, restriccion_fin: null, restriccion_url: null, restriccion_itinerarios: null,
                            area_descanso_id: null, arqueta_id: null, baliza_id: null, barrera_id: null, cartel_id: null, dotacion_p_id: null, dotacion_l_id: null, deposito_id: null,
                            direccional_id: null, grupo_instala_recreativa_id: null, instalacion_recreativa_id: null, marca_id: null, mesa_id: null, panel_id: null,
                            zona_aparcamiento_id: null, tramo_id: null, gestor_id: null, territorio_id: null, municipio_id: null, uuid: null,
                            created_user: null, created_date: null, last_edited_user: null, last_edited_date: null, validationstatus: null
                        };

                        ToAdd[contador].attributes.codigo = 'I-00000';
                        ToAdd[contador].attributes.grupo_incidencia_id = this.inc_grupo;
                        ToAdd[contador].attributes.subgrupo_incidencia_id = this.inc_motivo;
                        ToAdd[contador].attributes.nombre = ' ';
                        ToAdd[contador].attributes.incidencia_prioridad_id = this.inc_prioridad;
                        ToAdd[contador].attributes.descripcion = this.inc_descricion;
                        ToAdd[contador].attributes.incidencia_tipo_accion_id = this.inc_accion;
                        ToAdd[contador].attributes.estado_incidencia_id = 1;
                        ToAdd[contador].attributes.notificacion = this.inc_notificacion;
                        ToAdd[contador].attributes.destinatario_ut = this.inc_d_territoriales;
                        ToAdd[contador].attributes.destinatario_st = 0;
                        ToAdd[contador].attributes.notificar_cecopin = this.inc_d_cecopin;
                        ToAdd[contador].attributes.restriccion = this.inc_restriccion;
                        ToAdd[contador].attributes.restriccion_inicio = Date.parse(this.inc_res_finicio);
                        ToAdd[contador].attributes.restriccion_fin = Date.parse(this.inc_res_fin);
                        ToAdd[contador].attributes.restriccion_url = this.inc_res_resolucion;
                        ToAdd[contador].attributes.restriccion_itinerarios = this.valores_seleccionados_ItinerarioAfectado[j];
                        ToAdd[contador].attributes.territorio_id = 1;
                        ToAdd[contador].attributes.municipio_id = 1;
                        ToAdd[contador].attributes.uuid = codigo_uuid;
                        ToAdd[contador].attributes.validationstatus = 0;

                        if (grupoIncidencia == '5') {
                            ToAdd[contador].attributes.gestor_id = this.inc_gestor;
                            ToAdd[contador].setGeometry(puntos_Total[i]);
                        } else {
                            ToAdd[contador].attributes.gestor_id = 1;
                            ToAdd[contador].setGeometry(this.selected_eq_f[i].features.geometry);
                        }

                        //Distinguir entre el tipo de entidad seleccionada y su subtipo
                        if (grupoIncidencia == '2') {  //Area de descanso
                            ToAdd[contador].attributes.area_descanso_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '7') {  //Arquera
                            ToAdd[contador].attributes.arqueta_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '9') {  //Barrera
                            ToAdd[contador].attributes.barrera_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '8') {  //Deposito
                            ToAdd[contador].attributes.deposito_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '31') {  //Dotaciones
                            ToAdd[contador].attributes.dotacion_p_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '3') {  //Instalaciones recreativas
                            if (this.layerIncidencias[this.selected_eq_f[i].index] == "Instalaciones recreativas") {
                                ToAdd[contador].attributes.instalacion_recreativa_id = this.selected_eq_f[i].features.attributes.globalid;
                            } else if (this.layerIncidencias[this.selected_eq_f[i].index] == "Grupo de instalaciones recreativas") {
                                ToAdd[contador].attributes.grupo_instala_recreativa_id = this.selected_eq_f[i].features.attributes.globalid;
                            }
                        } else if (grupoIncidencia == '20') {  //Señal
                            ToAdd[contador].attributes.baliza_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '21') {
                            ToAdd[contador].attributes.cartel_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '22') {
                            ToAdd[contador].attributes.direccional_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '23') {
                            ToAdd[contador].attributes.marca_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '24') {
                            ToAdd[contador].attributes.mesa_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '25') {
                            ToAdd[contador].attributes.panel_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '4') {  //Zona aparcamiento
                            ToAdd[contador].attributes.zona_aparcamiento_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '5') {  //Viario
                            ToAdd[contador].attributes.tramo_id = this.selected_eq_f[i].features.attributes.globalid;
                        }
                        contador++;
                    }
                }

                tabla_incidencia.applyEdits(ToAdd, null, null, (resultInc) => {
                    if (this.selected_eq_f.length == 1) {
                        this.inc_objectid = resultInc[0].objectId;
                        var arrPromises = [];
                        for (var f = 0; f < this._uploadField.files.length; f++) {
                            var formData = new FormData();
                            formData.append("attachment", this._uploadField.files[f]);
                            var tabla_incidencia_att = new FeatureLayer(this.url_incidencia);
                            var p = tabla_incidencia_att.addAttachment(this.inc_objectid, formData);
                            arrPromises.push(p);
                        }
                        Promise.all(arrPromises).then(result => {
                            this.finLoading();
                            document.getElementById('loading_msg').innerHTML = 'Incidencias almacenadas correctamente.';
                            document.getElementById('loading_msg').className = 't2';
                            document.getElementById('end_cib').style.display = 'block';
                            document.getElementById('btn_at_fi_5_cib').style.display = 'none';
                        }).catch(error => {
                            this.finLoading();
                            document.getElementById('loading_msg').innerHTML = 'Se produjo un error al adjuntar el archivo, int�ntelo de nuevo';
                            document.getElementById('loading_msg').className = 't2_red';
                            document.getElementById('btn_at_fi_5').style.display = 'block';
                        });
                    } else {
                        this.finLoading();
                        document.getElementById('loading_msg').innerHTML = 'Incidencias almacenadas correctamente.';
                        document.getElementById('loading_msg').className = 't2';
                        document.getElementById('end_cib').style.display = 'block';
                        document.getElementById('btn_at_fi_5_cib').style.display = 'none';
                    }
                },
                    function (error) {
                        this.finLoading();
                        document.getElementById('loading_msg').innerHTML = 'Se produjo un error al guardar los datos de Incidencias, int�ntelo de nuevo';
                        document.getElementById('loading_msg').className = 't2_red';
                        document.getElementById('btn_at_fi_5_cib').style.display = 'block';
                    })
            },

            create_new_incidence_utm: function () {
                this.loading();
                document.getElementById('loading_msg_utm').innerHTML = "Guardando Incidencias...";
                var grupoIncidencia = document.getElementById("grupo_inc_utm").value;
                var codigo_uuid = this.uuid();

                if (grupoIncidencia == '5') {
                    var puntos_Total = [];

                    for (var i = 0; i < this.selected_eq_f.length; i++) {
                        var puntos_i = new Point();
                        puntos_i.spatialReference = this.map.spatialReference
                        puntos_i.x = this.centroideViario[i][0]
                        puntos_i.y = this.centroideViario[i][1]
                        puntos_Total.push(puntos_i)
                    }
                }

                var tabla_incidencia = new FeatureLayer(this.url_incidencia);
                var ToAdd = []
                var contador = 0;
                if (this.valores_seleccionados_ItinerarioAfectado.length == 0) {
                    this.valores_seleccionados_ItinerarioAfectado = [0];
                }
                for (var i = 0; i < this.selected_eq_f.length; i++) { //Recorrer los elementos seleccionados
                    for (var j = 0; j < this.valores_seleccionados_ItinerarioAfectado.length; j++) {
                        ToAdd[contador] = new Graphic();
                        ToAdd[contador].attributes = {
                            objectid: null, globalid: null, codigo: null, grupo_incidencia_id: null, subgrupo_incidencia_id: null, nombre: null, incidencia_prioridad_id: null,
                            descripcion: null, incidencia_tipo_accion_id: null, estado_incidencia_id: null, notificacion: null, destinatario_ut: null, destinatario_st: null, notificar_cecopin: null,
                            restriccion: null, restriccion_inicio: null, restriccion_fin: null, restriccion_url: null, restriccion_itinerarios: null,
                            area_descanso_id: null, arqueta_id: null, baliza_id: null, barrera_id: null, cartel_id: null, dotacion_p_id: null, dotacion_l_id: null, deposito_id: null,
                            direccional_id: null, grupo_instala_recreativa_id: null, instalacion_recreativa_id: null, marca_id: null, mesa_id: null, panel_id: null,
                            zona_aparcamiento_id: null, tramo_id: null, gestor_id: null, territorio_id: null, municipio_id: null, uuid: null,
                            created_user: null, created_date: null, last_edited_user: null, last_edited_date: null, validationstatus: null
                        };

                        ToAdd[contador].attributes.codigo = 'I-00000';
                        ToAdd[contador].attributes.grupo_incidencia_id = this.inc_grupo;
                        ToAdd[contador].attributes.subgrupo_incidencia_id = this.inc_motivo;
                        ToAdd[contador].attributes.nombre = ' ';
                        ToAdd[contador].attributes.incidencia_prioridad_id = this.inc_prioridad;
                        ToAdd[contador].attributes.descripcion = this.inc_descricion;
                        ToAdd[contador].attributes.incidencia_tipo_accion_id = this.inc_accion;
                        ToAdd[contador].attributes.estado_incidencia_id = 1;
                        ToAdd[contador].attributes.notificacion = this.inc_notificacion;
                        ToAdd[contador].attributes.destinatario_ut = this.inc_d_territoriales;
                        ToAdd[contador].attributes.destinatario_st = 0;
                        ToAdd[contador].attributes.notificar_cecopin = this.inc_d_cecopin;
                        ToAdd[contador].attributes.restriccion = this.inc_restriccion;
                        ToAdd[contador].attributes.restriccion_inicio = Date.parse(this.inc_res_finicio);
                        ToAdd[contador].attributes.restriccion_fin = Date.parse(this.inc_res_fin);
                        ToAdd[contador].attributes.restriccion_url = this.inc_res_resolucion;
                        ToAdd[contador].attributes.restriccion_itinerarios = this.valores_seleccionados_ItinerarioAfectado[j];
                        ToAdd[contador].attributes.territorio_id = 1;
                        ToAdd[contador].attributes.municipio_id = 1;
                        ToAdd[contador].attributes.uuid = codigo_uuid;
                        ToAdd[contador].attributes.validationstatus = 0;

                        if (grupoIncidencia == '5') {
                            ToAdd[contador].attributes.gestor_id = this.inc_gestor;
                            ToAdd[contador].setGeometry(puntos_Total[i]);
                        } else {
                            ToAdd[contador].attributes.gestor_id = 1;
                            ToAdd[contador].setGeometry(this.selected_eq_f[i].features.geometry);
                        }

                        //Distinguir entre el tipo de entidad seleccionada y su subtipo
                        if (grupoIncidencia == '2') {  //Area de descanso
                            ToAdd[contador].attributes.area_descanso_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '7') {  //Arquera
                            ToAdd[contador].attributes.arqueta_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '9') {  //Barrera
                            ToAdd[contador].attributes.barrera_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '8') {  //Deposito
                            ToAdd[contador].attributes.deposito_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '31') {  //Dotaciones
                            ToAdd[contador].attributes.dotacion_p_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '3') {  //Instalaciones recreativas
                            if (this.layerIncidencias[this.selected_eq_f[i].index] == "Instalaciones recreativas") {
                                ToAdd[contador].attributes.instalacion_recreativa_id = this.selected_eq_f[i].features.attributes.globalid;
                            } else if (this.layerIncidencias[this.selected_eq_f[i].index] == "Grupo de instalaciones recreativas") {
                                ToAdd[contador].attributes.grupo_instala_recreativa_id = this.selected_eq_f[i].features.attributes.globalid;
                            }
                        } else if (grupoIncidencia == '20') {  //Señal
                            ToAdd[contador].attributes.baliza_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '21') {
                            ToAdd[contador].attributes.cartel_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '22') {
                            ToAdd[contador].attributes.direccional_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '23') {
                            ToAdd[contador].attributes.marca_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '24') {
                            ToAdd[contador].attributes.mesa_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '25') {
                            ToAdd[contador].attributes.panel_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '4') {  //Zona aparcamiento
                            ToAdd[contador].attributes.zona_aparcamiento_id = this.selected_eq_f[i].features.attributes.globalid;
                        } else if (grupoIncidencia == '5') {  //Viario
                            ToAdd[contador].attributes.tramo_id = this.selected_eq_f[i].features.attributes.globalid;
                        }
                        contador++;
                    }
                }

                tabla_incidencia.applyEdits(ToAdd, null, null, (resultInc) => {
                    if (this.selected_eq_f.length == 1) {
                        this.inc_objectid = resultInc[0].objectId;
                        var arrPromises = [];
                        for (var f = 0; f < this._uploadField.files.length; f++) {
                            var formData = new FormData();
                            formData.append("attachment", this._uploadField.files[f]);
                            var tabla_incidencia_att = new FeatureLayer(this.url_incidencia);
                            var p = tabla_incidencia_att.addAttachment(this.inc_objectid, formData);
                            arrPromises.push(p);
                        }
                        Promise.all(arrPromises).then(result => {
                            this.finLoading();
                            document.getElementById('loading_msg_utm').innerHTML = 'Incidencias almacenadas correctamente.';
                            document.getElementById('loading_msg_utm').className = 't2';
                            document.getElementById('end_utm').style.display = 'block';
                            document.getElementById('btn_at_fi_5_utm').style.display = 'none';
                        }).catch(error => {
                            this.finLoading();
                            document.getElementById('loading_msg_utm').innerHTML = 'Se produjo un error al adjuntar el archivo, int�ntelo de nuevo';
                            document.getElementById('loading_msg_utm').className = 't2_red';
                            document.getElementById('btn_at_fi_5_utm').style.display = 'block';
                        });
                    } else {
                        this.finLoading();
                        document.getElementById('loading_msg_utm').innerHTML = 'Incidencias almacenadas correctamente.';
                        document.getElementById('loading_msg_utm').className = 't2';
                        document.getElementById('end_utm').style.display = 'block';
                        document.getElementById('btn_at_fi_5_utm').style.display = 'none';
                    }
                },
                    function (error) {
                        this.finLoading();
                        document.getElementById('loading_msg_utm').innerHTML = 'Se produjo un error al guardar los datos de Incidencias, int�ntelo de nuevo';
                        document.getElementById('loading_msg_utm').className = 't2_red';
                        document.getElementById('btn_at_fi_5_utm').style.display = 'block';
                    })
            },

            Relations: function (related_layers_url, related_layers_id, related_layers_title, related_layers_group_id) {
                this.related_layers_url = related_layers_url;
                this.related_layers_id = related_layers_id;
                this.related_layers_title = related_layers_title;
                this.related_layers_group_id = related_layers_group_id;
            },

            Tabla_localizador: function (posicion, globalid) {
                this.posicion = posicion;
                this.globalid = globalid;
            },

            uuid: function () {
                return '{xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx}'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                });
            },

            Entidad: function (features, index) {
                this.features = features;
                this.index = index;
            }

        });
    });
